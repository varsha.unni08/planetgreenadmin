package com.pgpavers.admin.webservice;

import android.content.Context;

import com.pgpavers.admin.data.appconstants.Literals;
import com.pgpavers.admin.data.preferencehelper.PreferenceHelper;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitHelper {

    private static Retrofit retrofit;
    private static final String BASE_URL = "http://www.centroidsolutions.in/6_grace/adm_api/";

    public static Retrofit getRetrofitInstance(final Context context) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder().readTimeout(1, TimeUnit.MINUTES).connectTimeout(1,TimeUnit.MINUTES);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("Authorization", "Bearer "+new PreferenceHelper(context).getData(Literals.Logintokenkey))

                        .method(original.method(), original.body())
                        .build();
                return chain.proceed(request);
            }
        });




        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;
    }
}
