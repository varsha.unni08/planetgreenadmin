package com.pgpavers.admin.webservice;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.pgpavers.admin.data.pojo.AdminOrders;
import com.pgpavers.admin.data.pojo.ArpDiscount;
import com.pgpavers.admin.data.pojo.BranchCustomerOutstand;
import com.pgpavers.admin.data.pojo.Branchdata;
import com.pgpavers.admin.data.pojo.CustomerOutstand;
import com.pgpavers.admin.data.pojo.DiscountSales;
import com.pgpavers.admin.data.pojo.InVoice;
import com.pgpavers.admin.data.pojo.OutStandBranch;
import com.pgpavers.admin.data.pojo.OutStandingCustomer;
import com.pgpavers.admin.data.pojo.Pattern;
import com.pgpavers.admin.data.pojo.Products;
import com.pgpavers.admin.data.pojo.Purchase;
import com.pgpavers.admin.data.pojo.SalesByBranch;
import com.pgpavers.admin.data.pojo.SalesCollection;
import com.pgpavers.admin.data.pojo.SalesPerson;
import com.pgpavers.admin.data.pojo.SalesPersonOutStanding;
import com.pgpavers.admin.data.pojo.SalesPersonWiseSales;
import com.pgpavers.admin.data.pojo.StatusMsg;
import com.pgpavers.admin.data.pojo.StockData;
import com.pgpavers.admin.data.pojo.SupplierByBranch;
import com.pgpavers.admin.data.pojo.SupplierData;
import com.pgpavers.admin.data.pojo.ViewTargetDate;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface WebInterfaceimpl {

    @FormUrlEncoded
    @POST("Login.php")
    Call<JsonObject> getLoginData(@Field("user")String user, @Field("password")String password);

    @GET("adm_patterns.php")
    Call<List<Pattern>>getPatters();

    @GET("adm_product_stock.php")
    Call<List<StockData>>getProductStock(@Query("cd_pattern")String cd_pattern);

    @GET("adm_product_stock.php")
    Call<List<Branchdata>>getBranchData(@Query("cd_product")String cd_product);

    @GET("adm_all_branches.php")
    Call<List<Branchdata>>getAllBranches();

    @GET("adm_get_salesperson.php")
    Call<List<SalesPerson>>getAllSalesPersons();

    @GET("adm_view_orders.php")
    Call<List<AdminOrders>>getAdminOrders(@Query("branch")String branch,@Query("cd_sperson")String cd_sperson);

    @GET("adm_approve_sales_order.php?apicall=approve")
    Call<List<StatusMsg>>approveSalesOrder(@Query("cd_sales_order")String cd_sales_order);

    @GET("adm_approve_sales_order.php?apicall=cancel")
    Call<List<StatusMsg>>deleteSalesOrder(@Query("cd_sales_order")String cd_sales_order);

    @GET("adm_production_details.php")
    Call<JsonArray>getAdminProductionDetails(@Query("from")String from,@Query("to")String to);

    @GET("adm_all_products.php")
    Call<List<Products>>getAllProducts();


    @GET("adm_production_details.php")
    Call<JsonArray>getAdminProductionDetailsProductWise(@Query("from")String from,@Query("to")String to,@Query("cd_item")String cd_item);



    @GET("customer_outstanding.php?apicall=group")
    Call<List<OutStandingCustomer>>customerOutstanding();


    @GET("customer_outstanding.php?apicall=branch")
    Call<List<OutStandBranch>>getBranchOutStand();


    @GET("customer_outstanding.php?apicall=branch")
    Call<List<BranchCustomerOutstand>>getBranchOutStandByID(@Query("cd_branch")String cdbranch);

    @GET("customer_outstanding.php?apicall=group")
    Call<List<SalesPersonOutStanding>>getSalesPersonOutstanding(@Query("cd_sales_person")String cd_sales_person);


    @GET("customer_outstanding.php?apicall=group")
    Call<List<CustomerOutstand>>getSalesCustomerOutStand(@Query("cd_sales_person")String cd_sales_person, @Query("cd_sales_person1")String cd_sales);


    @GET("customer_outstanding.php?apicall=group")
    Call<List<InVoice>>getInvoice(@Query("cd_customer")String cd_customer);

    @GET("customer_outstanding.php?apicall=branch")
    Call<List<InVoice>>getBranchInvoice(@Query("cd_branch")String cdbranch,@Query("cd_customer")String cd_customer);


    @GET("supplier_outstanding.php")
    Call<List<SupplierData>>getSupplierData();

    @GET("supplier_outstanding.php")
    Call<List<SupplierByBranch>>getSupplierDataByBranchid(@Query("cd_branch")String cdbranch);

    @GET("adm_purchase_details.php")
    Call<List<Purchase>>getAllPurchaseDetails(@Query("cd_branch")String cdbranch,@Query("from")String from,@Query("to")String to);


    @GET("adm_discount_details.php")
    Call<List<DiscountSales>>getDiscountdetails(@Query("cd_sales_person")String cd_sales_person, @Query("from")String from, @Query("to")String to);



    @GET("adm_discount_details.php")
    Call<List<DiscountSales>>getDiscountdetailsbyBranch(@Query("cd_branch")String cd_branch, @Query("from")String from, @Query("to")String to);


    @GET("adm_arp.php?apicall=salesperson")
    Call<ArpDiscount>getARPSales(@Query("cd_sales_person")String cd_sales_person, @Query("from")String from, @Query("to")String to);


    @GET("adm_arp.php?apicall=branch")
    Call<ArpDiscount>getARPBranch(@Query("cd_branch")String cd_branch, @Query("from")String from, @Query("to")String to);


    @GET("adm_sales_details.php?apicall=salesperson")
    Call<List<SalesPersonWiseSales>>getSalesBySalesPerson(@Query("cd_sales_person")String cd_sales_person,@Query("from")String from,@Query("to")String to);


    @GET("adm_sales_details.php?apicall=branch")
    Call<List<SalesByBranch>>salesByBranch(@Query("from")String from, @Query("to")String to, @Query("cd_branch")String cd_branch);




    @FormUrlEncoded
    @POST("adm_set_targets.php")
    Call<JsonObject>setTarget(@Field("target_type")String target_type,@Field("cd_sales_person")String cd_sales_person,@Field("vl_target_in_rupees")String vl_target_in_rupees,@Field("dt_start_target")String dt_start_target,@Field("dt_end_target")String dt_end_target);

    @GET("adm_view_target_achieve.php")
    Call<List<ViewTargetDate>>getViewTargets(@Query("start_date")String start_date,@Query("end_date")String end_date,  @Query("target_type")String target_type);





    @GET("adm_view_target_achieve.php")
    Call<JsonArray>getTargets(@Query("start_date")String start_date,@Query("end_date")String end_date,  @Query("target_type")String target_type);




    @GET("adm_collection_details.php")
    Call<List<SalesCollection>>getSalesCollectiondetails(@Query("cd_sales_person")String cd_sales_person,@Query("from")String from,@Query("to")String to);


    @GET("adm_collection_details.php")
    Call<List<SalesCollection>>getBranchCollections(@Query("cd_branch")String cd_branch,@Query("from")String from,@Query("to")String to);

}
