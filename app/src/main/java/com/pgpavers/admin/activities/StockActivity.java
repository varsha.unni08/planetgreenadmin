package com.pgpavers.admin.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.pgpavers.admin.R;
import com.pgpavers.admin.adapters.StockAdapter;
import com.pgpavers.admin.connectivity.NetConnection;
import com.pgpavers.admin.data.pojo.BranchListData;
import com.pgpavers.admin.data.pojo.Branchdata;
import com.pgpavers.admin.data.pojo.Pattern;
import com.pgpavers.admin.data.pojo.StockData;
import com.pgpavers.admin.fragments.StockBranchFragment;
import com.pgpavers.admin.fragments.StockPatternFragment;
import com.pgpavers.admin.progress.ProgressFragment;
import com.pgpavers.admin.webservice.RetrofitHelper;
import com.pgpavers.admin.webservice.WebInterfaceimpl;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StockActivity extends AppCompatActivity {

    ImageView imgback,imgdropdown;

    ProgressFragment progressFragment;
    TextView txtStock,txtNodata;

    List<Pattern>patterns;
    RecyclerView recycler_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        txtStock=findViewById(R.id.txtStock);
        txtNodata=findViewById(R.id.txtNodata);
        imgdropdown=findViewById(R.id.imgdropdown);
        recycler_view=findViewById(R.id.recycler_view);
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();

            }
        });


        txtStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(patterns!=null) {
                    if (patterns.size() > 0) {

                        StockPatternFragment stockPatternFragment=new StockPatternFragment();
                        stockPatternFragment.setPatterns(patterns);
stockPatternFragment.show(getSupportFragmentManager(),"dflkm");
                    }
                }

            }
        });

        imgdropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(patterns!=null) {

                    if (patterns.size() > 0) {

                        StockPatternFragment stockPatternFragment=new StockPatternFragment();
                        stockPatternFragment.setPatterns(patterns);
                        stockPatternFragment.show(getSupportFragmentManager(),"dflkm");


                    }
                }
            }
        });



        showStockPattern();
    }


    public void showStockPattern()
    {
       progressFragment=new ProgressFragment();
       progressFragment.show(getSupportFragmentManager(),"dfjk");

        WebInterfaceimpl webInterfaceimpl= RetrofitHelper.getRetrofitInstance(StockActivity.this).create(WebInterfaceimpl.class);
        Call<List<Pattern>>jsonObjectCall=webInterfaceimpl.getPatters();
        jsonObjectCall.enqueue(new Callback<List<Pattern>>() {
            @Override
            public void onResponse(Call<List<Pattern>> call, Response<List<Pattern>> response) {
                progressFragment.dismiss();

patterns=response.body();










            }

            @Override
            public void onFailure(Call<List<Pattern>> call, Throwable t) {
                progressFragment.dismiss();


                if(!NetConnection.isConnected(StockActivity.this))
                {
                    Toast.makeText(StockActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });


    }

    public void callStock(Pattern pattern)
    {

        txtStock.setText(pattern.getDsPattern());

        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"dfjk");

        WebInterfaceimpl webInterfaceimpl= RetrofitHelper.getRetrofitInstance(StockActivity.this).create(WebInterfaceimpl.class);
        Call<List<StockData>>jsonObjectCall=webInterfaceimpl.getProductStock(pattern.getCdPattern());
        jsonObjectCall.enqueue(new Callback<List<StockData>>() {
            @Override
            public void onResponse(Call<List<StockData>> call, Response<List<StockData>> response) {
                progressFragment.dismiss();
                if(response.body().size()>0) {

                    txtNodata.setVisibility(View.GONE);
                    recycler_view.setVisibility(View.VISIBLE);


                    StockAdapter stockAdapter = new StockAdapter(StockActivity.this, response.body());
                    recycler_view.setLayoutManager(new LinearLayoutManager(StockActivity.this));
                    recycler_view.setAdapter(stockAdapter);
                }
                else {

                    txtNodata.setVisibility(View.VISIBLE);
                    recycler_view.setVisibility(View.GONE);
                }


            }

            @Override
            public void onFailure(Call<List<StockData>> call, Throwable t) {
                progressFragment.dismiss();


                if(!NetConnection.isConnected(StockActivity.this))
                {
                    Toast.makeText(StockActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });

    }


    public void showStockBranch(String id)
    {

        progressFragment=new ProgressFragment();

        progressFragment.show(getSupportFragmentManager(),"dfjk");

        WebInterfaceimpl webInterfaceimpl= RetrofitHelper.getRetrofitInstance(StockActivity.this).create(WebInterfaceimpl.class);
        Call<List<Branchdata>>jsonObjectCall=webInterfaceimpl.getBranchData(id);
        jsonObjectCall.enqueue(new Callback<List<Branchdata>>() {
            @Override
            public void onResponse(Call<List<Branchdata>> call, Response<List<Branchdata>> response) {
                progressFragment.dismiss();

                StockBranchFragment stockBranchFragment=new StockBranchFragment();

             BranchListData branchListData=   new BranchListData();
             branchListData.setBranchdata(response.body());

                Bundle bundle=new Bundle();
                bundle.putSerializable("data",branchListData);
                stockBranchFragment.setArguments(bundle);
                stockBranchFragment.show(getSupportFragmentManager(),"sdjkk");







            }

            @Override
            public void onFailure(Call<List<Branchdata>> call, Throwable t) {
                progressFragment.dismiss();


                if(!NetConnection.isConnected(StockActivity.this))
                {
                    Toast.makeText(StockActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });


    }
}
