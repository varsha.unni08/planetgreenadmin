package com.pgpavers.admin.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.pgpavers.admin.R
import kotlinx.android.synthetic.main.activity_login.*

import com.pgpavers.admin.webservice.RetrofitHelperLogin
import com.pgpavers.admin.webservice.WebInterfaceimpl
import retrofit2.Callback
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.pgpavers.admin.connectivity.NetConnection
import com.pgpavers.admin.data.appconstants.Literals
import com.pgpavers.admin.data.pojo.LoginToken
import com.pgpavers.admin.data.preferencehelper.PreferenceHelper
import com.pgpavers.admin.progress.ProgressFragment


import retrofit2.Call
import retrofit2.Response
import java.lang.Exception


class LoginActivity : AppCompatActivity() {

    var progressFragment: ProgressFragment? = null;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar!!.hide()

        btnlogin.setOnClickListener {

            if (!edtPhone.text.toString().equals("")) {

                if (!edtPassword.text.toString().equals("")) {

                    progressFragment = ProgressFragment();
                    progressFragment!!.show(supportFragmentManager, "kcmk")
                    val service = RetrofitHelperLogin.getRetrofitInstance()
                        .create(WebInterfaceimpl::class.java)
                    val call =
                        service.getLoginData(edtPhone.text.toString(), edtPassword.text.toString())

                    call.enqueue(object : Callback<JsonObject> {

                        override fun onResponse(
                            call: Call<JsonObject>,
                            response: Response<JsonObject>
                        ) {



                            Log.e("Tag", response.body().toString())
                            var loginToken = Gson().fromJson<LoginToken>(
                                response.body(),
                                LoginToken::class.java
                            )
                            try {

                                if (loginToken.status == 1) {


                                    progressFragment!!.dismiss()

                                    PreferenceHelper(this@LoginActivity).putData(
                                        Literals.Logintokenkey,
                                        loginToken.token
                                    )

                                    val intent =
                                        Intent(this@LoginActivity, MainActivity::class.java)
                                    startActivity(intent)
                                    finish()
                                } else {
                                    progressFragment!!.dismiss()
                                    Toast.makeText(
                                        this@LoginActivity,
                                        loginToken.message,
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            } catch (e: Exception) {

                            }


                        }

                        override fun onFailure(call: Call<JsonObject>, t: Throwable) {

if(!NetConnection.isConnected(this@LoginActivity))
{
    Toast.makeText(this@LoginActivity,"Check Internet connection",Toast.LENGTH_SHORT).show()
}




                        }
                    })


                }


            }
        }
    }
}
