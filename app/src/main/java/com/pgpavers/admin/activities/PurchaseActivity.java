package com.pgpavers.admin.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pgpavers.admin.R;
import com.pgpavers.admin.adapters.PurchaseAdapter;
import com.pgpavers.admin.connectivity.NetConnection;
import com.pgpavers.admin.data.pojo.Branchdata;
import com.pgpavers.admin.data.pojo.Purchase;
import com.pgpavers.admin.progress.ProgressFragment;
import com.pgpavers.admin.webservice.RetrofitHelper;
import com.pgpavers.admin.webservice.WebInterfaceimpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PurchaseActivity extends AppCompatActivity {

    ImageView imgback;
    AppCompatImageView imgDropdownBranch, imgpickFromDate, imgpickToDate;
    TextView txtBranch,txtName, txtStartdate, txttoDate,txtNodata;

    RecyclerView recycler_view;
    ProgressFragment progressFragment;

    String start_date = "", end_date = "";

    Button btnSubmit;

    Branchdata branch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);
        getSupportActionBar().hide();

        imgback = findViewById(R.id.imgback);
        imgDropdownBranch = findViewById(R.id.imgDropdownBranch);
        txtBranch = findViewById(R.id.txtBranch);
        btnSubmit = findViewById(R.id.btnSubmit);
        recycler_view = findViewById(R.id.recycler_view);

        imgpickFromDate = findViewById(R.id.imgpickFromDate);
        imgpickToDate = findViewById(R.id.imgpickToDate);

        txtStartdate = findViewById(R.id.txtStartdate);
        txttoDate = findViewById(R.id.txttoDate);
        txtNodata=findViewById(R.id.txtNodata);

        txtName=findViewById(R.id.txtName);


        txtBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getAllBranches();
            }
        });

        imgDropdownBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getAllBranches();
            }
        });


        imgpickToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);

            }
        });

        imgpickFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(1);

            }
        });

        txtStartdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);

            }
        });
        txttoDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);

            }
        });


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(branch!=null) {

                    if (!start_date.equals("")) {

                        if (!end_date.equals("")) {

                            Date sta_date = null;
                            Date end_dat = null;


                            try {

                                DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
                                sta_date = dateFormat.parse(start_date);
                                end_dat = dateFormat.parse(end_date);

                                if (sta_date.before(end_dat)) {

                              showPurchaselist();


                                } else {
                                    Toast.makeText(PurchaseActivity.this, "Select date properly", Toast.LENGTH_SHORT).show();

                                }


                            } catch (Exception e) {
                                Toast.makeText(PurchaseActivity.this, e.toString(), Toast.LENGTH_SHORT).show();

                            }


                        } else {
                            Toast.makeText(PurchaseActivity.this, "select end date", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(PurchaseActivity.this, "select start date", Toast.LENGTH_SHORT).show();
                    }
                }

                else {
                    Toast.makeText(PurchaseActivity.this, "select a branch", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


    public void showDatePicker(final int start) {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(PurchaseActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                // txtDob.setText();

                int m = i1 + 1;

                if (start == 1) {
                    start_date = i + "-" + m + "-" + i2;
                    txtStartdate.setText(start_date);
                } else {
                    end_date = i + "-" + m + "-" + i2;
                    txttoDate.setText(end_date);
                }


            }
        }, year, month, day);

        datePickerDialog.show();
    }

    public void showPurchaselist() {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "dfjk");

        WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(PurchaseActivity.this).create(WebInterfaceimpl.class);


        Call<List<Purchase>>listCall=webInterfaceimpl.getAllPurchaseDetails(branch.getCdBranch(),start_date,end_date);
        listCall.enqueue(new Callback<List<Purchase>>() {
            @Override
            public void onResponse(Call<List<Purchase>> call, Response<List<Purchase>> response) {
                progressFragment.dismiss();

                if(response.body()!=null) {

                    if(response.body().size()>0) {

                        List<Purchase> purchases = new ArrayList<>();
                        purchases.addAll(response.body());

                        txtName.setText("Total purchase : " + purchases.get(response.body().size()-1).getTotalPurchase());
                        purchases.remove(response.body().size()-1);


                        if(purchases.size()>0) {

                            recycler_view.setVisibility(View.VISIBLE);
                            txtNodata.setVisibility(View.GONE);


                            recycler_view.setLayoutManager(new LinearLayoutManager(PurchaseActivity.this));
                            recycler_view.setAdapter(new PurchaseAdapter(PurchaseActivity.this, purchases));
                        }
                        else {
                            recycler_view.setVisibility(View.GONE);
                            txtNodata.setVisibility(View.VISIBLE);

                        }

                    }
                    else {

                        recycler_view.setVisibility(View.VISIBLE);
                        txtNodata.setVisibility(View.GONE);

                    }





                }
                }

            @Override
            public void onFailure(Call<List<Purchase>> call, Throwable t) {
                progressFragment.dismiss();
                if(!NetConnection.isConnected(PurchaseActivity.this))
                {
                    Toast.makeText(PurchaseActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }



    public void getAllBranches()
    {
        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"dfjk");

        WebInterfaceimpl webInterfaceimpl= RetrofitHelper.getRetrofitInstance(PurchaseActivity.this).create(WebInterfaceimpl.class);
        Call<List<Branchdata>> jsonObjectCall=webInterfaceimpl.getAllBranches();
        jsonObjectCall.enqueue(new Callback<List<Branchdata>>() {
            @Override
            public void onResponse(Call<List<Branchdata>> call, Response<List<Branchdata>> response) {
                progressFragment.dismiss();

                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {

                        Branchdata branchda=new Branchdata();
                        branchda.setCdBranch("0");
                        branchda.setDsBranch("All branches");


                        List<Branchdata>branchdata=new ArrayList<>();
                        branchdata.addAll(response.body());
                        branchdata.add(0,branchda);

                        showAllBranchesAlert(branchdata);

                    }
                    else {

                        Toast.makeText(PurchaseActivity.this,"No branches found",Toast.LENGTH_SHORT).show();
                    }
                }


            }

            @Override
            public void onFailure(Call<List<Branchdata>> call, Throwable t) {
                progressFragment.dismiss();
                if(!NetConnection.isConnected(PurchaseActivity.this))
                {
                    Toast.makeText(PurchaseActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    public void showAllBranchesAlert(final List<Branchdata> branchdata)
    {
        final List<String>strings=new ArrayList<>();

        AlertDialog.Builder builder = new AlertDialog.Builder(PurchaseActivity.this);
        builder.setTitle("Choose a Branch");
        ;
        for (Branchdata st:branchdata)
        {
            strings.add(st.getDsBranch());
        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtBranch.setText(branchdata.get(which).getDsBranch());

                branch=branchdata.get(which);

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}