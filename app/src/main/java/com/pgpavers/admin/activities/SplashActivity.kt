package com.pgpavers.admin.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.pgpavers.admin.R
import com.pgpavers.admin.data.appconstants.Literals
import com.pgpavers.admin.data.preferencehelper.PreferenceHelper

class SplashActivity : AppCompatActivity() {
    private var mDelayHandler: Handler? = null
    private val SPLASH_DELAY: Long = 3000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        supportActionBar!!.hide()

        val runnable= Runnable {


            if(PreferenceHelper(this).getData(Literals.Logintokenkey).equals("")) {

                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
            else{
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()

            }
        }

        mDelayHandler=Handler()
        mDelayHandler!!.postDelayed(runnable,3000)


    }
}
