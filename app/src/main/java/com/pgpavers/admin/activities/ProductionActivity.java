package com.pgpavers.admin.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.pgpavers.admin.R;
import com.pgpavers.admin.adapters.ProductionAdapter;
import com.pgpavers.admin.connectivity.NetConnection;
import com.pgpavers.admin.data.pojo.Productiondata;
import com.pgpavers.admin.data.pojo.Products;
import com.pgpavers.admin.progress.ProgressFragment;
import com.pgpavers.admin.webservice.RetrofitHelper;
import com.pgpavers.admin.webservice.WebInterfaceimpl;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductionActivity extends AppCompatActivity {

    ImageView imgback,imgpickFromDate,imgpickToDate;


    TextView txtFromDb,txttoDate;

    String start_date="",end_date="";

    TextView txtNodata,txtTotalCount;
    Button btnSubmit;

    String qty="";
    RecyclerView recycler_view;
    ProgressFragment progressFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_production);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        imgpickFromDate=findViewById(R.id.imgpickFromDate);
        imgpickToDate=findViewById(R.id.imgpickToDate);

        txtFromDb=findViewById(R.id.txtFromDb);
        txttoDate=findViewById(R.id.txttoDate);
        txtNodata=findViewById(R.id.txtNodata);
        btnSubmit=findViewById(R.id.btnSubmit);
        txtTotalCount=findViewById(R.id.txtTotalCount);
        recycler_view=findViewById(R.id.recycler_view);





        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!start_date.equals(""))
                {

                    if(!end_date.equals(""))
                    {

                        Date sta_date=null;
                        Date end_dat=null;


                        try{

                            DateFormat dateFormat=new SimpleDateFormat("yyyy-mm-dd");
                            sta_date=dateFormat.parse(start_date);
                            end_dat=dateFormat.parse(end_date);

                            if(sta_date.before(end_dat))
                            {

                                showProductionlist();


                            }
                            else {
                                Toast.makeText(ProductionActivity.this,"Select date properly",Toast.LENGTH_SHORT).show();

                            }






                        }catch (Exception e)
                        {
                            Toast.makeText(ProductionActivity.this,e.toString(),Toast.LENGTH_SHORT).show();

                        }










                    }
                    else {
                        Toast.makeText(ProductionActivity.this,"select end date",Toast.LENGTH_SHORT).show();
                    }

                }
                else {
                    Toast.makeText(ProductionActivity.this,"select start date",Toast.LENGTH_SHORT).show();
                }

            }
        });


        imgpickToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);

            }
        });

        imgpickFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(1);

            }
        });

        txtFromDb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);

            }
        });
        txttoDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);

            }
        });



        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });

    }



    public void showDatePicker(final int start)
    {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(ProductionActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                // txtDob.setText();

                int m=i1+1;

                if(start==1) {
                    start_date = i + "-" + m + "-" + i2;
                    txtFromDb.setText(start_date);
                }
                else {
                    end_date = i + "-" + m + "-" + i2;
                    txttoDate.setText(end_date);
                }


            }
        }, year, month, day);

        datePickerDialog.show();
    }


    public void showProductionlist()
    {
        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"dfjk");

        WebInterfaceimpl webInterfaceimpl= RetrofitHelper.getRetrofitInstance(ProductionActivity.this).create(WebInterfaceimpl.class);

        Call<JsonArray>jsonArrayCall=webInterfaceimpl.getAdminProductionDetails(start_date,end_date);
        jsonArrayCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

                progressFragment.dismiss();

                try {

                    List<Productiondata>productiondata=new ArrayList<>();

                    if (response.body() != null) {

                        if (response.body().size() > 0) {

                            JSONArray jsonArray = new JSONArray(response.body().toString());

                            for (int i = 0; i <jsonArray.length();i++)
                            {

                                if (i == 0) {

                                    JSONObject jsonObject=jsonArray.getJSONObject(i);
                                    if(jsonObject.getInt("status")==1)
                                    {

                                         qty=jsonObject.getString("groupwise_produced_qty");
                                        txtTotalCount.setText("Grand Total : "+qty);

                                    }
                                    else {

                                        String msg=jsonObject.getString("message");
                                        Toast.makeText(ProductionActivity.this, msg, Toast.LENGTH_SHORT).show();


                                        break;
                                    }

                                }
                                else {

//
                                    Productiondata productiondata1=new Productiondata();


                                    JSONObject jsonObject=jsonArray.getJSONObject(i);
                               productiondata1.setDsBranch(jsonObject.getString("ds_branch"));;
                               productiondata1.setDsBranchCode(jsonObject.getString("ds_branch_code"));

                               productiondata1.setBranchwiseProducedQty(jsonObject.getInt("branchwise_produced_qty"));

productiondata.add(productiondata1);

                                }


                            }

                            if(productiondata.size()>0)
                            {

                                recycler_view.setVisibility(View.VISIBLE);
                                txtNodata.setVisibility(View.GONE);
                                txtTotalCount.setVisibility(View.VISIBLE);

                                ProductionAdapter productionAdapter=new ProductionAdapter(ProductionActivity.this,productiondata);
                                recycler_view.setLayoutManager(new LinearLayoutManager(ProductionActivity.this));
                                recycler_view.setAdapter(productionAdapter);



                            }
                            else {

                                recycler_view.setVisibility(View.GONE);
                                txtNodata.setVisibility(View.VISIBLE);
                                txtTotalCount.setVisibility(View.GONE);
                            }



                        } else {

                            Toast.makeText(ProductionActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                        }


                    }

                }catch (Exception e)
                {

                }

            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {

                progressFragment.dismiss();
                if(!NetConnection.isConnected(ProductionActivity.this))
                {
                    Toast.makeText(ProductionActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });


    }


    public void getSelectedData(Productiondata productiondata)
    {

        Intent intent=new Intent(ProductionActivity.this,ProductiondetailsActivity.class);
        intent.putExtra("data",productiondata);
        intent.putExtra("qty",qty);
        intent.putExtra("startdate",start_date);
        intent.putExtra("enddate",end_date);


        startActivity(intent);


    }
}
