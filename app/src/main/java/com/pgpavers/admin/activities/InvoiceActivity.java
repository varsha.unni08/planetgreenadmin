package com.pgpavers.admin.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pgpavers.admin.R;
import com.pgpavers.admin.adapters.InvoiceAdapter;
import com.pgpavers.admin.connectivity.NetConnection;
import com.pgpavers.admin.data.pojo.BranchCustomerOutstand;
import com.pgpavers.admin.data.pojo.CustomerOutstand;
import com.pgpavers.admin.data.pojo.InVoice;
import com.pgpavers.admin.progress.ProgressFragment;
import com.pgpavers.admin.webservice.RetrofitHelper;
import com.pgpavers.admin.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvoiceActivity extends AppCompatActivity {

    ImageView imgback;

    ProgressFragment progressFragment;

    CustomerOutstand customerOutstand;

    RecyclerView recycler_view;
    TextView txtNodata,txtName;
    BranchCustomerOutstand branchCustomerOutstand;

    String branchid="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        recycler_view=findViewById(R.id.recycler_view);
        txtNodata=findViewById(R.id.txtNodata);
        txtName=findViewById(R.id.txtName);

        customerOutstand=(CustomerOutstand)getIntent().getSerializableExtra("customerid");
        branchCustomerOutstand=(BranchCustomerOutstand) getIntent().getSerializableExtra("branchcustomer");

        branchid=getIntent().getStringExtra("branchid");
        if(customerOutstand!=null) {
            getInvoice();
        }

        if(branchCustomerOutstand!=null)
        {
            getInvoiceByBranch();

        }
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
    }

    public void getInvoiceByBranch()
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "dfjk");

        WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(InvoiceActivity.this).create(WebInterfaceimpl.class);

        Call<List<InVoice>>listCall=webInterfaceimpl.getBranchInvoice(branchid,branchCustomerOutstand.getCdCustomer());

        listCall.enqueue(new Callback<List<InVoice>>() {
            @Override
            public void onResponse(Call<List<InVoice>> call, Response<List<InVoice>> response) {
                progressFragment.dismiss();

                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {

                        List<InVoice>inVoices=new ArrayList<>();
                        inVoices.addAll(response.body());





                        if(inVoices.get(0).getInvoiceDetailsOf()!=null) {
                            txtName.setText(inVoices.get(0).getInvoiceDetailsOf());
                            inVoices.remove(0);

                            recycler_view.setVisibility(View.VISIBLE);
                            recycler_view.setLayoutManager(new LinearLayoutManager(InvoiceActivity.this));

                            recycler_view.setAdapter(new InvoiceAdapter(InvoiceActivity.this,inVoices));
                            txtNodata.setVisibility(View.GONE);


                        }






                    }


                }


            }

            @Override
            public void onFailure(Call<List<InVoice>> call, Throwable t) {
                progressFragment.dismiss();
                if (!NetConnection.isConnected(InvoiceActivity.this)) {
                    Toast.makeText(InvoiceActivity.this, "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void getInvoice()
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "dfjk");

        WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(InvoiceActivity.this).create(WebInterfaceimpl.class);

        Call<List<InVoice>>listCall=webInterfaceimpl.getInvoice(customerOutstand.getCdCustomer());
        listCall.enqueue(new Callback<List<InVoice>>() {
            @Override
            public void onResponse(Call<List<InVoice>> call, Response<List<InVoice>> response) {
                progressFragment.dismiss();

                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {

                        List<InVoice>inVoices=new ArrayList<>();
                        inVoices.addAll(response.body());





                        if(inVoices.get(0).getInvoiceDetailsOf()!=null) {
                            txtName.setText(inVoices.get(0).getInvoiceDetailsOf());
                            inVoices.remove(0);

                            recycler_view.setVisibility(View.VISIBLE);
                            recycler_view.setLayoutManager(new LinearLayoutManager(InvoiceActivity.this));

                            recycler_view.setAdapter(new InvoiceAdapter(InvoiceActivity.this,inVoices));
                            txtNodata.setVisibility(View.GONE);


                        }






                    }


                }


            }

            @Override
            public void onFailure(Call<List<InVoice>> call, Throwable t) {
                progressFragment.dismiss();
                if (!NetConnection.isConnected(InvoiceActivity.this)) {
                    Toast.makeText(InvoiceActivity.this, "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
