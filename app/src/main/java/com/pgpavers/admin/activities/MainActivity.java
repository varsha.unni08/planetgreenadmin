package com.pgpavers.admin.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.adapters.Homeadapter;
import com.pgpavers.admin.data.appconstants.Literals;
import com.pgpavers.admin.data.preferencehelper.PreferenceHelper;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ImageView imgLogout,imgarp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().hide();
        recyclerView=findViewById(R.id.recyclerview);
        imgLogout=findViewById(R.id.imgLogout);
        imgarp=findViewById(R.id.imgarp);

        imgarp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,ARPActivity.class));
            }
        });



        imgLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(R.string.app_name);
                builder.setMessage("Do you want to logout now ?");
                builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        new PreferenceHelper(MainActivity.this).clearData();
                        startActivity(new Intent(MainActivity.this,LoginActivity.class));
                        finish();


                    }
                });

                builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                    }
                });
                builder.show();



            }
        });
        setAdapter();

    }

    public void setAdapter()
    {

        Homeadapter homeadapter=new Homeadapter(MainActivity.this, Literals.arr_constants);
        recyclerView.setLayoutManager(new GridLayoutManager(MainActivity.this,3));
        recyclerView.setAdapter(homeadapter);

    }
}
