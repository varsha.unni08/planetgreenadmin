package com.pgpavers.admin.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pgpavers.admin.R;
import com.pgpavers.admin.adapters.AdminOrderAdapter;
import com.pgpavers.admin.connectivity.NetConnection;
import com.pgpavers.admin.data.pojo.AdminOrders;
import com.pgpavers.admin.data.pojo.Branchdata;
import com.pgpavers.admin.data.pojo.Pattern;
import com.pgpavers.admin.data.pojo.SalesPerson;
import com.pgpavers.admin.progress.ProgressFragment;
import com.pgpavers.admin.webservice.RetrofitHelper;
import com.pgpavers.admin.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdersActivity extends AppCompatActivity {

    ImageView imgback;

    AppCompatImageView imgDropdownBranch,imgDropdownSales;

    TextView txtBranch,txtSales;
    RecyclerView recycler_view;
    ProgressFragment progressFragment;
    Button btnSubmit;

    Branchdata branch;

    SalesPerson salesPerson;
    TextView txtNodata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        imgDropdownBranch=findViewById(R.id.imgDropdownBranch);
        txtBranch=findViewById(R.id.txtBranch);
        recycler_view=findViewById(R.id.recycler_view);
        imgDropdownSales=findViewById(R.id.imgDropdownSales);
        txtSales=findViewById(R.id.txtSales);
        txtNodata=findViewById(R.id.txtNodata);
        btnSubmit=findViewById(R.id.btnSubmit);


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(branch!=null)
                {

                    if(salesPerson!=null)
                    {

getAllOrders();

                    }
                    else {

                        Toast.makeText(OrdersActivity.this,"Select sales person",Toast.LENGTH_SHORT).show();
                    }



                }
                else {

                    Toast.makeText(OrdersActivity.this,"Select branch",Toast.LENGTH_SHORT).show();
                }
            }
        });

        txtSales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showAllSalesPersons();
            }
        });

        imgDropdownSales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showAllSalesPersons();

            }
        });


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });

        txtBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAllBranches();
            }
        });
        imgDropdownBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAllBranches();
            }
        });

       //
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }


    public void getAllBranches()
    {
        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"dfjk");

        WebInterfaceimpl webInterfaceimpl= RetrofitHelper.getRetrofitInstance(OrdersActivity.this).create(WebInterfaceimpl.class);
        Call<List<Branchdata>> jsonObjectCall=webInterfaceimpl.getAllBranches();
        jsonObjectCall.enqueue(new Callback<List<Branchdata>>() {
            @Override
            public void onResponse(Call<List<Branchdata>> call, Response<List<Branchdata>> response) {
                progressFragment.dismiss();

                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {

                        Branchdata branchda=new Branchdata();
                        branchda.setCdBranch("0");
                        branchda.setDsBranch("All branches");


                        List<Branchdata>branchdata=new ArrayList<>();
                        branchdata.addAll(response.body());
                        branchdata.add(0,branchda);

                        showAllBranchesAlert(branchdata);

                    }
                    else {

                        Toast.makeText(OrdersActivity.this,"No branches found",Toast.LENGTH_SHORT).show();
                    }
                }


            }

            @Override
            public void onFailure(Call<List<Branchdata>> call, Throwable t) {
                progressFragment.dismiss();
                if(!NetConnection.isConnected(OrdersActivity.this))
                {
                    Toast.makeText(OrdersActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    public void showAllSalesPersons()
    {
        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"dfjk");

        WebInterfaceimpl webInterfaceimpl= RetrofitHelper.getRetrofitInstance(OrdersActivity.this).create(WebInterfaceimpl.class);
        Call<List<SalesPerson>> jsonObjectCall=webInterfaceimpl.getAllSalesPersons();
        jsonObjectCall.enqueue(new Callback<List<SalesPerson>>() {
            @Override
            public void onResponse(Call<List<SalesPerson>> call, Response<List<SalesPerson>> response) {

                progressFragment.dismiss();

                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {

                        List<SalesPerson>salesPeople=new ArrayList<>();
                        salesPeople.addAll(response.body());

                        SalesPerson salesPerson1=new SalesPerson();
                        salesPerson1.setCdSalesPerson("0");
                        salesPerson1.setDsSalesPerson("All sales persons");
                        salesPeople.add(0,salesPerson1);
                        showAllSalesPersonAlert(salesPeople);

                    }
                    else {

                        Toast.makeText(OrdersActivity.this,"No branches found",Toast.LENGTH_SHORT).show();
                    }
                }


            }

            @Override
            public void onFailure(Call<List<SalesPerson>> call, Throwable t) {
                progressFragment.dismiss();
                if(!NetConnection.isConnected(OrdersActivity.this))
                {
                    Toast.makeText(OrdersActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void showAllSalesPersonAlert(final List<SalesPerson>salesPeople)
    {
        final List<String>strings=new ArrayList<>();

        AlertDialog.Builder builder = new AlertDialog.Builder(OrdersActivity.this);
        builder.setTitle("Choose a Sales Person");
        ;
        for (SalesPerson st:salesPeople)
        {
            strings.add(st.getDsSalesPerson());
        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtSales.setText(salesPeople.get(which).getDsSalesPerson());

                salesPerson=salesPeople.get(which);

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void showAllBranchesAlert(final List<Branchdata> branchdata)
    {
        final List<String>strings=new ArrayList<>();

        AlertDialog.Builder builder = new AlertDialog.Builder(OrdersActivity.this);
        builder.setTitle("Choose a Branch");
        ;
        for (Branchdata st:branchdata)
        {
            strings.add(st.getDsBranch());
        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

             txtBranch.setText(branchdata.get(which).getDsBranch());

             branch=branchdata.get(which);

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void getAllOrders()
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"dfjk");

        WebInterfaceimpl webInterfaceimpl= RetrofitHelper.getRetrofitInstance(OrdersActivity.this).create(WebInterfaceimpl.class);
        Call<List<AdminOrders>> jsonObjectCall=webInterfaceimpl.getAdminOrders(branch.getCdBranch(),salesPerson.getCdSalesPerson());
        jsonObjectCall.enqueue(new Callback<List<AdminOrders>>() {
            @Override
            public void onResponse(Call<List<AdminOrders>> call, Response<List<AdminOrders>> response) {
                progressFragment.dismiss();

                if(response.body()!=null)
                {
                    if(response.body().size()>0)
                    {
                        if(response.body().get(0).getCdBranch()!=null) {

                            txtNodata.setVisibility(View.GONE);
                            recycler_view.setVisibility(View.VISIBLE);

                            recycler_view.setLayoutManager(new LinearLayoutManager(OrdersActivity.this));
                            recycler_view.setAdapter(new AdminOrderAdapter(OrdersActivity.this, response.body()));

                        }
                        else {

                            txtNodata.setVisibility(View.VISIBLE);
                            recycler_view.setVisibility(View.GONE);

                            Toast.makeText(OrdersActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                        }
                    }
                    else {

                        txtNodata.setVisibility(View.VISIBLE);
                        recycler_view.setVisibility(View.GONE);

                        Toast.makeText(OrdersActivity.this,"No data found",Toast.LENGTH_SHORT).show();
                    }

                }
                else {

                    txtNodata.setVisibility(View.VISIBLE);
                    recycler_view.setVisibility(View.GONE);

                    Toast.makeText(OrdersActivity.this,"No data found",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<AdminOrders>> call, Throwable t) {
                progressFragment.dismiss();
                if(!NetConnection.isConnected(OrdersActivity.this))
                {
                    Toast.makeText(OrdersActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
