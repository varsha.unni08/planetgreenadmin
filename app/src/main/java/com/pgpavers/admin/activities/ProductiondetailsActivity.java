package com.pgpavers.admin.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.pgpavers.admin.R;
import com.pgpavers.admin.connectivity.NetConnection;
import com.pgpavers.admin.data.pojo.Productiondata;
import com.pgpavers.admin.data.pojo.Products;
import com.pgpavers.admin.fragments.ProductFragment;
import com.pgpavers.admin.progress.ProgressFragment;
import com.pgpavers.admin.webservice.RetrofitHelper;
import com.pgpavers.admin.webservice.WebInterfaceimpl;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductiondetailsActivity extends AppCompatActivity {

    ImageView imgback;
    AppCompatImageView imgpickarrow;
    TextView txtProduct;

    ProgressFragment progressFragment;

    Productiondata productiondata;

    TextView txtQty,txtbranchdetails,txtproductdetails;
    String qty="",startdate="",enddate="";

    Products prod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productiondetails);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);

        imgpickarrow=findViewById(R.id.imgpickarrow);

        txtProduct=findViewById(R.id.txtProduct);
        txtQty=findViewById(R.id.txtQty);
        txtbranchdetails=findViewById(R.id.txtbranchdetails);
        txtproductdetails=findViewById(R.id.txtproductdetails);


        productiondata=(Productiondata) getIntent().getSerializableExtra("data");
        qty=getIntent().getStringExtra("qty");



        startdate=getIntent().getStringExtra("startdate");
        enddate=getIntent().getStringExtra("enddate");

        txtQty.setText("Grand total : "+qty);
        txtbranchdetails.setText("Branch code : "+productiondata.getDsBranchCode()+"\n"+
                "Branch : "+productiondata.getDsBranch()+"\n"+
                "Total produced quantity : "+productiondata.getBranchwiseProducedQty());



        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });


        imgpickarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showAllProducts();



            }
        });



        txtProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showAllProducts();

            }
        });
    }


    public void showAllProducts()
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"dfjk");

        WebInterfaceimpl webInterfaceimpl= RetrofitHelper.getRetrofitInstance(ProductiondetailsActivity.this).create(WebInterfaceimpl.class);

        Call<List<Products>> listCall=webInterfaceimpl.getAllProducts();

        listCall.enqueue(new Callback<List<Products>>() {
            @Override
            public void onResponse(Call<List<Products>> call, Response<List<Products>> response) {

                progressFragment.dismiss();


                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {


                        showProductAlert(response.body());


                    }


                }



            }

            @Override
            public void onFailure(Call<List<Products>> call, Throwable t) {

                progressFragment.dismiss();
                if(!NetConnection.isConnected(ProductiondetailsActivity.this))
                {
                    Toast.makeText(ProductiondetailsActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });






    }

    public void showProductAlert(final List<Products>products)
    {

        ProductFragment productFragment=new ProductFragment();
        productFragment.setProducts(products);
        productFragment.show(getSupportFragmentManager(),"djkj");

    }


    public void getProductwiseDetails(Products products)
    {

        this.prod=products;

        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"dfjk");

        WebInterfaceimpl webInterfaceimpl= RetrofitHelper.getRetrofitInstance(ProductiondetailsActivity.this).create(WebInterfaceimpl.class);

        Call<JsonArray>jsonArrayCall=webInterfaceimpl.getAdminProductionDetailsProductWise(startdate,enddate,prod.getCdProduct());

        jsonArrayCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

                progressFragment.dismiss();

                try {

                    if (response.body() != null) {

                        if (response.body().size() > 0) {

                            JSONArray jsonArray=new JSONArray(response.body().toString());
                            JSONObject jsonObject=jsonArray.getJSONObject(0);
                            if(jsonObject.has("status"))
                            {
                                int code=jsonObject.getInt("status");

                                if(code==0)
                                {
                                    Toast.makeText(ProductiondetailsActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();

                                }
                            }
                            else {

                                txtproductdetails.setText("Product : "+jsonObject.getString("ds_product")+"\n"+
                                        "Total Production : "+jsonObject.getInt("total_production"));
                            }




                        }


                    }
                }catch (Exception e)
                {

                }



            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {

                progressFragment.dismiss();
                if(!NetConnection.isConnected(ProductiondetailsActivity.this))
                {
                    Toast.makeText(ProductiondetailsActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

}
