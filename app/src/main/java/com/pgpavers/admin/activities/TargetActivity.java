package com.pgpavers.admin.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.tabs.TabLayout;
import com.pgpavers.admin.R;
import com.pgpavers.admin.adapters.SalesTabPagerAdapter;
import com.pgpavers.admin.adapters.TargetTabpagerAdapter;

public class TargetActivity extends AppCompatActivity {

    ImageView imgback;
    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_target);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);

        tabLayout=findViewById(R.id.tab_layout);

        viewPager=findViewById(R.id.viewpager);


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        showTabs();
    }


    public void showTabs()
    {


        //String arr[]={"Set Target","View Target"};

        tabLayout.addTab(tabLayout.newTab().setText("Set Target"));
        tabLayout.addTab(tabLayout.newTab().setText("View Target"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager.setAdapter(new TargetTabpagerAdapter(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {


                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });




    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
