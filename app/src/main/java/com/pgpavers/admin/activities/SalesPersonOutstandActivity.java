package com.pgpavers.admin.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pgpavers.admin.R;
import com.pgpavers.admin.adapters.SalesPersonOutStandAdapter;
import com.pgpavers.admin.connectivity.NetConnection;
import com.pgpavers.admin.data.pojo.CustomerOutstand;
import com.pgpavers.admin.data.pojo.OutStandBranch;
import com.pgpavers.admin.data.pojo.OutStandingCustomer;
import com.pgpavers.admin.data.pojo.SalesPersonOutStanding;
import com.pgpavers.admin.fragments.SalesPersonFragment;
import com.pgpavers.admin.progress.ProgressFragment;
import com.pgpavers.admin.webservice.RetrofitHelper;
import com.pgpavers.admin.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesPersonOutstandActivity extends AppCompatActivity {

    OutStandingCustomer outStandingCustomer;

    ImageView imgback;

    RecyclerView recycler_view;
    TextView txtNodata;
    ProgressFragment progressFragment;

    int Mainadmin=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_person_outstand);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        recycler_view=findViewById(R.id.recycler_view);
        txtNodata=findViewById(R.id.txtNodata);

        outStandingCustomer=(OutStandingCustomer) getIntent().getSerializableExtra("Salesperson");
        Mainadmin=getIntent().getIntExtra("Mainadmin",0);

        if(outStandingCustomer!=null)
        {
            showSalesPersondata();
        }

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });


    }

    public void showSalesPersondata()
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "dfjk");

        WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(SalesPersonOutstandActivity.this).create(WebInterfaceimpl.class);

        Call<List<SalesPersonOutStanding>>listCall=null;

        if(Mainadmin==0) {

            listCall = webInterfaceimpl.getSalesPersonOutstanding(outStandingCustomer.getCdSalesPerson());
        }
        else {
            listCall = webInterfaceimpl.getSalesPersonOutstanding("0");

        }

        listCall.enqueue(new Callback<List<SalesPersonOutStanding>>() {
            @Override
            public void onResponse(Call<List<SalesPersonOutStanding>> call, Response<List<SalesPersonOutStanding>> response) {

                progressFragment.dismiss();
                if(response.body()!=null)
                {

                    if(response.body().size()>0) {

                        recycler_view.setVisibility(View.VISIBLE);
                        txtNodata.setVisibility(View.GONE);
                        recycler_view.setLayoutManager(new LinearLayoutManager(SalesPersonOutstandActivity.this));
                        recycler_view.setAdapter(new SalesPersonOutStandAdapter(SalesPersonOutstandActivity.this, response.body()));

                    }
                    else {

                        recycler_view.setVisibility(View.GONE);
                        txtNodata.setVisibility(View.VISIBLE);
                    }




                }




            }

            @Override
            public void onFailure(Call<List<SalesPersonOutStanding>> call, Throwable t) {

                progressFragment.dismiss();
                if (!NetConnection.isConnected(SalesPersonOutstandActivity.this)) {
                    Toast.makeText(SalesPersonOutstandActivity.this, "Check Internet connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    public void getSalesPersonOutstand(SalesPersonOutStanding salesPersonOutStanding)
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "dfjk");

        WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(SalesPersonOutstandActivity.this).create(WebInterfaceimpl.class);
        Call<List<CustomerOutstand>> listCall=webInterfaceimpl.getSalesCustomerOutStand(outStandingCustomer.getCdSalesPerson(),salesPersonOutStanding.getCdSalesPerson());
        listCall.enqueue(new Callback<List<CustomerOutstand>>() {
            @Override
            public void onResponse(Call<List<CustomerOutstand>> call, Response<List<CustomerOutstand>> response) {
                progressFragment.dismiss();
                if(response.body()!=null) {

                    if (response.body().size() > 0) {


                        showSalesPersonDialogue(response.body());


                    }

                }
            }

            @Override
            public void onFailure(Call<List<CustomerOutstand>> call, Throwable t) {
                progressFragment.dismiss();
                if (!NetConnection.isConnected(SalesPersonOutstandActivity.this)) {
                    Toast.makeText(SalesPersonOutstandActivity.this, "Check Internet connection", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }


    public void showSalesPersonDialogue(final List<CustomerOutstand>salesPersonOutStandings)
    {


        salesPersonOutStandings.remove(0);
        if(salesPersonOutStandings.size()>0) {

            SalesPersonFragment salesPersonFragment = new SalesPersonFragment();
            salesPersonFragment.setCustomerList(salesPersonOutStandings);
            salesPersonFragment.show(getSupportFragmentManager(), "sdlkm");
        }





    }

    public void getCustomerOutstand(CustomerOutstand customerOutstand)
    {

        Intent intent=new Intent(SalesPersonOutstandActivity.this,InvoiceActivity.class);
        intent.putExtra("customerid",customerOutstand);
        startActivity(intent);

    }
}
