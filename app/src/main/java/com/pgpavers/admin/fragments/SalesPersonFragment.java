package com.pgpavers.admin.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.activities.SalesPersonOutstandActivity;
import com.pgpavers.admin.adapters.CustomerOutstandAdapter;
import com.pgpavers.admin.adapters.CustomerSalesAdapter;
import com.pgpavers.admin.data.pojo.CustomerOutstand;
import com.pgpavers.admin.data.pojo.Pattern;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SalesPersonFragment extends DialogFragment {

    View view;
    EditText edtSearch;
    RecyclerView recycler_view;

    List<CustomerOutstand> customerList;
    List<CustomerOutstand> customerList_changed;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.layout_stockpattern,container,false);
        edtSearch=view.findViewById(R.id.edtSearch);
        recycler_view=view.findViewById(R.id.recycler_view);
        customerList_changed=new ArrayList<>();

        setCustomerSalesAdapter(customerList);

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {



                if(!charSequence.toString().equalsIgnoreCase(""))
                {




                    for (CustomerOutstand pattern:customerList) {

                        if(pattern.getDsCustomer().contains(charSequence.toString().toLowerCase(Locale.getDefault()))||pattern.getDsCustomer().toUpperCase(Locale.getDefault()).contains(charSequence.toString().toUpperCase(Locale.getDefault())))
                        {
                                customerList_changed.add(pattern);

                            }

                    }

                    setCustomerSalesAdapter(customerList_changed);


                }
                else {

                    setCustomerSalesAdapter(customerList);

                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return view;
    }


    public void setCustomerList(List<CustomerOutstand> customerLis)
    {



            this.customerList = customerLis;



    }

    public void setCustomerSalesAdapter(List<CustomerOutstand> customerLists)
    {
        if(customerLists.size()>0)
        {
            recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
            recycler_view.setAdapter(new CustomerSalesAdapter(getActivity(),customerLists,SalesPersonFragment.this));
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        Window window = getDialog().getWindow();
        if(window == null) return;
        WindowManager.LayoutParams params = window.getAttributes();
        params.width =  getActivity().getResources().getDisplayMetrics().widthPixels;;

        window.setAttributes(params);
    }


    public void getCustomerOutstand(CustomerOutstand customerOutstand)
    {

        ((SalesPersonOutstandActivity)getActivity()).getCustomerOutstand(customerOutstand);

    }
}
