package com.pgpavers.admin.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonArray;
import com.pgpavers.admin.R;
import com.pgpavers.admin.adapters.ViewTargetAdapter;
import com.pgpavers.admin.connectivity.NetConnection;
import com.pgpavers.admin.data.pojo.SalesPerson;
import com.pgpavers.admin.data.pojo.ViewTargetDate;
import com.pgpavers.admin.progress.ProgressFragment;
import com.pgpavers.admin.webservice.RetrofitHelper;
import com.pgpavers.admin.webservice.WebInterfaceimpl;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewTargetFragment extends Fragment {

    View view;

 DatePicker date_picker;
 Button date_time_set;

// TextView txtSsetTarget;
 AppCompatImageView imgpickFromDate,imgpickEndDate;
    AppCompatImageView imgDropdownSales;
    TextView txtSales;

    Integer month=0;
    Integer year=0;

    ProgressFragment progressFragment;

    String type="";
    Button btnsubmit;

    TextView txtdata,txtStartdate,txtEndDate;

    String date="",start_date="",end_date="";

    RecyclerView recycler_view;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

       view=inflater.inflate(R.layout.layout_viewtarget,container,false);
        date_picker=view.findViewById(R.id.date_picker);
        date_time_set=view.findViewById(R.id.date_time_set);

        imgpickFromDate=view.findViewById(R.id.imgpickFromDate);
        imgpickEndDate=view.findViewById(R.id.imgpickEndDate);
        txtStartdate=view.findViewById(R.id.txtStartdate);
        recycler_view=view.findViewById(R.id.recycler_view);

        txtEndDate=view.findViewById(R.id.txtEndDate);

        txtSales=view.findViewById(R.id.txtSales);
        btnsubmit=view.findViewById(R.id.btnsubmit);
        imgDropdownSales=view.findViewById(R.id.imgDropdownSales);
        txtdata=view.findViewById(R.id.txtdata);

        imgpickFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  ExpiryDialog();

                showDatePicker(1);
            }
        });

        txtStartdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ExpiryDialog();
                showDatePicker(1);
            }
        });

        imgpickEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);
            }
        });

        txtEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(0);

            }
        });

        txtSales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showAllSalesPersons();
            }
        });

        imgDropdownSales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showAllSalesPersons();
            }
        });

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (!start_date.equals("")) {

                    if (!end_date.equals("")) {

                        Date sta_date = null;
                        Date end_dat = null;


                        try {

                            DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
                            sta_date = dateFormat.parse(start_date);
                            end_dat = dateFormat.parse(end_date);

                            if (sta_date.before(end_dat)) {

                                if (!type.equals("")) {


                                    progressFragment = new ProgressFragment();
                                    progressFragment.show(getFragmentManager(), "dfjk");

                                    WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(getActivity()).create(WebInterfaceimpl.class);

                                    Call<List<ViewTargetDate>> viewTargets = webInterfaceimpl.getViewTargets(start_date, end_date, type);

                                    viewTargets.enqueue(new Callback<List<ViewTargetDate>>() {
                                        @Override
                                        public void onResponse(Call<List<ViewTargetDate>> call, Response<List<ViewTargetDate>> response) {
                                            progressFragment.dismiss();

                                            if (response.body() != null) {

                                                List<ViewTargetDate> viewTargetDates = new ArrayList<>();
                                                viewTargetDates.addAll(response.body());


                                                if(viewTargetDates.size()>0)
                                                {
                                                    int lastindex=viewTargetDates.size()-1;

                                                    txtdata.setText("Total acheivement : "+viewTargetDates.get(lastindex).getAchievementTotal()+"\nTotal target : "+viewTargetDates.get(lastindex).getTargetTotal());


                                                    viewTargetDates.remove(lastindex);

                                                    if(viewTargetDates.size()>0)
                                                    {
                                                        recycler_view.setVisibility(View.VISIBLE);
                                                    recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
                                                    recycler_view.setAdapter(new ViewTargetAdapter(getActivity(),viewTargetDates));
                                                    }
                                                    else {

                                                        recycler_view.setVisibility(View.GONE);
                                                        Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                                                    }

                                                }
                                                else {

                                                    recycler_view.setVisibility(View.GONE);
                                                    Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                                                }

//                                                if (viewTargetDates.get(0).getStatus() != 0) {
//
//                                                    showAllDateAlert(viewTargetDates);
//
//
//                                                } else {
//                                                    Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
//                                                }


                                            }


                                        }

                                        @Override
                                        public void onFailure(Call<List<ViewTargetDate>> call, Throwable t) {
                                            progressFragment.dismiss();
                                            if (!NetConnection.isConnected(getActivity())) {
                                                Toast.makeText(getActivity(), "Check Internet connection", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });


                                } else {

                                    Toast.makeText(getActivity(), "select a sales person", Toast.LENGTH_SHORT).show();

                                }

                            } else {

                                Toast.makeText(getActivity(), "select date properly", Toast.LENGTH_SHORT).show();

                            }
                        } catch (Exception e) {

                        }

                    } else {

                        Toast.makeText(getActivity(), "select end date", Toast.LENGTH_SHORT).show();

                    }
                }
                else {

                    Toast.makeText(getActivity(), "select start date", Toast.LENGTH_SHORT).show();

                }



            }
        });


        return view;
    }


    public void showDatePicker(final int start) {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                // txtDob.setText();

                int m = i1 + 1;

                if (start == 1) {
                    start_date = i + "-" + m + "-" + i2;
                    txtStartdate.setText(start_date);
                } else {
                    end_date = i + "-" + m + "-" + i2;
                    txtEndDate.setText(end_date);
                }


            }
        }, year, month, day);

        datePickerDialog.show();
    }


//    private void ExpiryDialog() {
//
//        final Dialog dialog = new Dialog(getActivity());
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_date_picker);
//        dialog.show();
//
//        Calendar mCalendar=Calendar.getInstance();
//
//        final DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.date_picker);
//        date_time_set = (Button) dialog.findViewById(R.id.date_time_set);
//        datePicker.init(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH), null);
//
//        LinearLayout ll = (LinearLayout) datePicker.getChildAt(0);
//        LinearLayout ll2 = (LinearLayout) ll.getChildAt(0);
//        ll2.getChildAt(0).setVisibility(View.INVISIBLE);
//
//        date_time_set.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View view){
//                dialog.dismiss();
//                 month = datePicker.getMonth()+1;
//                 year = datePicker.getYear();
//               String expMonth = (month.toString().length()   == 1 ? "0"+month.toString():month.toString());
//              String  expYear = year.toString();
////                etExpiryDate.setText(expMonth + "/" + expYear);
//
//                txtSsetTarget.setText(expMonth+"/"+expYear);
//            }
//        });
//    }


    public void showAllSalesPersons()
    {

        showAllSalesPersonAlert();
//        progressFragment=new ProgressFragment();
//        progressFragment.show(getFragmentManager(),"dfjk");
//
//        WebInterfaceimpl webInterfaceimpl= RetrofitHelper.getRetrofitInstance(getActivity()).create(WebInterfaceimpl.class);
//        Call<List<SalesPerson>> jsonObjectCall=webInterfaceimpl.getAllSalesPersons();
//        jsonObjectCall.enqueue(new Callback<List<SalesPerson>>() {
//            @Override
//            public void onResponse(Call<List<SalesPerson>> call, Response<List<SalesPerson>> response) {
//
//                progressFragment.dismiss();
//
//                if(response.body()!=null)
//                {
//
//                    if(response.body().size()>0)
//                    {
//
//                        List<SalesPerson>salesPeople=new ArrayList<>();
//                        salesPeople.addAll(response.body());
//
//                        SalesPerson salesPerson1=new SalesPerson();
//                        salesPerson1.setCdSalesPerson("0");
//                        salesPerson1.setDsSalesPerson("All sales persons");
//                        salesPeople.add(0,salesPerson1);
//
//                        showAllSalesPersonAlert(salesPeople);
//
//                    }
//                    else {
//
//                        Toast.makeText(getActivity(),"No Sales person found",Toast.LENGTH_SHORT).show();
//                    }
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<List<SalesPerson>> call, Throwable t) {
//                progressFragment.dismiss();
//                if(!NetConnection.isConnected(getActivity()))
//                {
//                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
    }


    public void showAllSalesPersonAlert()
    {
//        final List<String>strings=new ArrayList<>();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a type");
        ;
//        for (SalesPerson st:salesPeople)
//        {
//            strings.add(st.getDsSalesPerson());
//        }

        final String arr[]={"sale","Collection"};


        builder.setItems(arr, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtSales.setText(arr[which]);

                int a=which+1;

                type=String.valueOf(a);

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


//    public void showAllDateAlert(final List<ViewTargetDate>viewTargetDates)
//    {
//        final List<String>strings=new ArrayList<>();
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle("Choose a date range");
//        ;
//        for (ViewTargetDate st:viewTargetDates)
//        {
//            strings.add(st.getDtRecord());
//        }
//
//
//        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//                dialog.dismiss();
//
//
//date=strings.get(which);
//showTotalTarget();
//
//
//            }
//        });
//
//        AlertDialog dialog = builder.create();
//        dialog.show();
//    }

    public void showTotalTarget()
    {
        progressFragment=new ProgressFragment();
        progressFragment.show(getFragmentManager(),"dfjk");

        WebInterfaceimpl webInterfaceimpl= RetrofitHelper.getRetrofitInstance(getActivity()).create(WebInterfaceimpl.class);

        Call<JsonArray>jsonArrayCall=webInterfaceimpl.getTargets(start_date,end_date,type);
        jsonArrayCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                progressFragment.dismiss();

                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {

                        try {


                            JSONArray jsonArray=new JSONArray(response.body().toString());
                            for (int i=0;i<jsonArray.length();i++)
                            {
                                JSONObject jsonObject=jsonArray.getJSONObject(i);
                                txtdata.setText("Target : "+jsonObject.getString("Target")+"\nAchievement : "+jsonObject.getInt("Achievement"));

                            }




                        }catch (Exception e)
                        {


                        }




                    }




                }




            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                progressFragment.dismiss();
                if(!NetConnection.isConnected(getActivity()))
                {
                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

}
