package com.pgpavers.admin.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.activities.InvoiceActivity;
import com.pgpavers.admin.activities.SalesActivity;
import com.pgpavers.admin.adapters.SupplierByBranchAdapter;
import com.pgpavers.admin.adapters.SupplierDataAdapter;
import com.pgpavers.admin.connectivity.NetConnection;
import com.pgpavers.admin.data.pojo.Branchdata;
import com.pgpavers.admin.data.pojo.SupplierByBranch;
import com.pgpavers.admin.data.pojo.SupplierData;
import com.pgpavers.admin.progress.ProgressFragment;
import com.pgpavers.admin.webservice.RetrofitHelper;
import com.pgpavers.admin.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SupplierFragment extends Fragment {

    TextView txtName, txtNodata, txtBranch;
    RecyclerView recycler_view;

    ProgressFragment progressFragment;

    AppCompatImageView imgDropdownBranch;

    Branchdata branchdataSelected;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_supplier, container, false);
        txtName = view.findViewById(R.id.txtName);
        txtNodata = view.findViewById(R.id.txtNodata);
        recycler_view = view.findViewById(R.id.recycler_view);
        txtBranch = view.findViewById(R.id.txtBranch);
        imgDropdownBranch = view.findViewById(R.id.imgDropdownBranch);

        txtBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getAllBranches();

            }
        });

        imgDropdownBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getAllBranches();

            }
        });


        setupSupplierData("");


        return view;
    }


    public void setupSupplierData(String cd_branch) {
        progressFragment = new ProgressFragment();
        progressFragment.show(getFragmentManager(), "dfjk");



        if (cd_branch.equals("")) {

            WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(getActivity()).create(WebInterfaceimpl.class);

            Call<List<SupplierData>> listCall = webInterfaceimpl.getSupplierData();

            listCall.enqueue(new Callback<List<SupplierData>>() {
                @Override
                public void onResponse(Call<List<SupplierData>> call, Response<List<SupplierData>> response) {

                    progressFragment.dismiss();


                    List<SupplierData> supplierData = new ArrayList<>();
                    supplierData.addAll(response.body());


                    if (supplierData.size() > 0) {
                        if (supplierData.get(0).getStatus() == 1) {
                            txtName.setText("Total Advance : " + supplierData.get(0).getTotalAdvance() + "\n" +
                                    "Total Balance : " + supplierData.get(0).getTotalBalance());
                            supplierData.remove(0);

                            recycler_view.setVisibility(View.VISIBLE);
                            txtName.setVisibility(View.VISIBLE);
                            txtNodata.setVisibility(View.GONE);
                            recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
                            recycler_view.setAdapter(new SupplierDataAdapter(getActivity(), supplierData));


                        }


                    }


                }

                @Override
                public void onFailure(Call<List<SupplierData>> call, Throwable t) {
                    progressFragment.dismiss();
                    if (!NetConnection.isConnected(getActivity())) {
                        Toast.makeText(getActivity(), "Check Internet connection", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {

            WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(getActivity()).create(WebInterfaceimpl.class);

            Call<List<SupplierByBranch>> listCall = webInterfaceimpl.getSupplierDataByBranchid(cd_branch);


            listCall.enqueue(new Callback<List<SupplierByBranch>>() {
                @Override
                public void onResponse(Call<List<SupplierByBranch>> call, Response<List<SupplierByBranch>> response) {

                    progressFragment.dismiss();

                    if(response.body()!=null) {

                        List<SupplierByBranch> supplierByBranches = new ArrayList<>();
                        supplierByBranches.addAll(response.body());

                        if (supplierByBranches.size() > 0) {

                            recycler_view.setVisibility(View.VISIBLE);
                            txtName.setVisibility(View.GONE);
                            txtNodata.setVisibility(View.GONE);
                            recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
                            recycler_view.setAdapter(new SupplierByBranchAdapter(getActivity(), supplierByBranches));


                        } else {

                            recycler_view.setVisibility(View.GONE);
                            txtName.setVisibility(View.GONE);
                            txtNodata.setVisibility(View.VISIBLE);
                        }


                    }
                    else {

                        recycler_view.setVisibility(View.GONE);
                        txtName.setVisibility(View.GONE);
                        txtNodata.setVisibility(View.VISIBLE);
                    }


                }

                @Override
                public void onFailure(Call<List<SupplierByBranch>> call, Throwable t) {
                    progressFragment.dismiss();
                    if (!NetConnection.isConnected(getActivity())) {
                        Toast.makeText(getActivity(), "Check Internet connection", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }


    }


    public void getAllBranches() {
        progressFragment = new ProgressFragment();
        progressFragment.show(getFragmentManager(), "dfjk");

        WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(getActivity()).create(WebInterfaceimpl.class);
        Call<List<Branchdata>> jsonObjectCall = webInterfaceimpl.getAllBranches();
        jsonObjectCall.enqueue(new Callback<List<Branchdata>>() {
            @Override
            public void onResponse(Call<List<Branchdata>> call, Response<List<Branchdata>> response) {
                progressFragment.dismiss();

                if (response.body() != null) {

                    if (response.body().size() > 0) {

                        Branchdata branchda=new Branchdata();
                        branchda.setCdBranch("0");
                        branchda.setDsBranch("All branches");


                        List<Branchdata>branchdata=new ArrayList<>();
                        branchdata.addAll(response.body());
                        branchdata.add(0,branchda);

                        showAllBranchesAlert(branchdata);

                    } else {

                        Toast.makeText(getActivity(), "No branches found", Toast.LENGTH_SHORT).show();
                    }
                }


            }

            @Override
            public void onFailure(Call<List<Branchdata>> call, Throwable t) {
                progressFragment.dismiss();
                if (!NetConnection.isConnected(getActivity())) {
                    Toast.makeText(getActivity(), "Check Internet connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    public void showAllBranchesAlert(final List<Branchdata> branchdata) {
        final List<String> strings = new ArrayList<>();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a Branch");
        ;
        for (Branchdata st : branchdata) {
            strings.add(st.getDsBranch());
        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtBranch.setText(branchdata.get(which).getDsBranch());

                branchdataSelected = branchdata.get(which);

                setupSupplierData(branchdataSelected.getCdBranch());

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
