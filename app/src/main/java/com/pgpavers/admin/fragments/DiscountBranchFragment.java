package com.pgpavers.admin.fragments;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;

import com.pgpavers.admin.R;
import com.pgpavers.admin.activities.PurchaseActivity;
import com.pgpavers.admin.connectivity.NetConnection;
import com.pgpavers.admin.data.pojo.Branchdata;
import com.pgpavers.admin.data.pojo.DiscountSales;
import com.pgpavers.admin.progress.ProgressFragment;
import com.pgpavers.admin.webservice.RetrofitHelper;
import com.pgpavers.admin.webservice.WebInterfaceimpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DiscountBranchFragment extends Fragment {

    TextView txtBranch;

    AppCompatImageView imgDropdownbranch;

    AppCompatImageView imgDropdownsales, imgpickFromDate, imgpickToDate;
    TextView txtSalesperson, txtStartdate, txttoDate,txtTotalCount;

    ProgressFragment progressFragment;

    Button btnSubmit;

    View view;

    Branchdata branch;

    String start_date = "", end_date = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view=inflater.inflate(R.layout.layout_discountbranch,container,false);

        imgDropdownbranch=view.findViewById(R.id.imgDropdownbranch);
        txtBranch=view.findViewById(R.id.txtBranch);
        imgpickFromDate = view.findViewById(R.id.imgpickFromDate);
        imgpickToDate = view.findViewById(R.id.imgpickToDate);

        txtStartdate = view.findViewById(R.id.txtStartdate);
        txttoDate = view.findViewById(R.id.txttoDate);
        btnSubmit=view.findViewById(R.id.btnSubmit);
        txtTotalCount=view.findViewById(R.id.txtTotalCount);


        imgpickToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);

            }
        });

        imgpickFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(1);

            }
        });

        txtStartdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);

            }
        });
        txttoDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);

            }
        });

        txtBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getAllBranches();

            }
        });

        imgDropdownbranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getAllBranches();

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(branch!=null) {

                    if (!start_date.equals("")) {

                        if (!end_date.equals("")) {

                            Date sta_date = null;
                            Date end_dat = null;


                            try {

                                DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
                                sta_date = dateFormat.parse(start_date);
                                end_dat = dateFormat.parse(end_date);

                                if (sta_date.before(end_dat)) {

                                //    showPurchaselist();

                                    showDiscountBranch();


                                } else {
                                    Toast.makeText(getActivity(), "Select date properly", Toast.LENGTH_SHORT).show();

                                }


                            } catch (Exception e) {
                                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();

                            }


                        } else {
                            Toast.makeText(getActivity(), "select end date", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(getActivity(), "select start date", Toast.LENGTH_SHORT).show();
                    }
                }

                else {
                    Toast.makeText(getActivity(), "select a branch", Toast.LENGTH_SHORT).show();
                }

            }
        });



        return view;
    }

    public void showDiscountBranch()
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getFragmentManager(),"dfjk");

        WebInterfaceimpl webInterfaceimpl= RetrofitHelper.getRetrofitInstance(getActivity()).create(WebInterfaceimpl.class);
        Call<List<DiscountSales>>listCall=webInterfaceimpl.getDiscountdetailsbyBranch(branch.getCdBranch(),start_date,end_date);

        listCall.enqueue(new Callback<List<DiscountSales>>() {
            @Override
            public void onResponse(Call<List<DiscountSales>> call, Response<List<DiscountSales>> response) {

                progressFragment.dismiss();

                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {

                        txtTotalCount.setText("Total Discount : "+response.body().get(0).getTotalDiscount());




                    }
                    else {

                        Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<List<DiscountSales>> call, Throwable t) {
                progressFragment.dismiss();
                if(!NetConnection.isConnected(getActivity()))
                {
                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    public void showDatePicker(final int start) {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                // txtDob.setText();

                int m = i1 + 1;

                if (start == 1) {
                    start_date = i + "-" + m + "-" + i2;
                    txtStartdate.setText(start_date);
                } else {
                    end_date = i + "-" + m + "-" + i2;
                    txttoDate.setText(end_date);
                }


            }
        }, year, month, day);

        datePickerDialog.show();
    }

    public void getAllBranches()
    {
        progressFragment=new ProgressFragment();
        progressFragment.show(getFragmentManager(),"dfjk");

        WebInterfaceimpl webInterfaceimpl= RetrofitHelper.getRetrofitInstance(getActivity()).create(WebInterfaceimpl.class);
        Call<List<Branchdata>> jsonObjectCall=webInterfaceimpl.getAllBranches();
        jsonObjectCall.enqueue(new Callback<List<Branchdata>>() {
            @Override
            public void onResponse(Call<List<Branchdata>> call, Response<List<Branchdata>> response) {
                progressFragment.dismiss();

                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {

                        Branchdata branchda=new Branchdata();
                        branchda.setCdBranch("0");
                        branchda.setDsBranch("All branches");


                        List<Branchdata>branchdata=new ArrayList<>();
                        branchdata.addAll(response.body());
                        branchdata.add(0,branchda);

                        showAllBranchesAlert(branchdata);

                    }
                    else {

                        Toast.makeText(getActivity(),"No branches found",Toast.LENGTH_SHORT).show();
                    }
                }


            }

            @Override
            public void onFailure(Call<List<Branchdata>> call, Throwable t) {
                progressFragment.dismiss();
                if(!NetConnection.isConnected(getActivity()))
                {
                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    public void showAllBranchesAlert(final List<Branchdata> branchdata)
    {
        final List<String>strings=new ArrayList<>();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a Branch");
        ;
        for (Branchdata st:branchdata)
        {
            strings.add(st.getDsBranch());
        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtBranch.setText(branchdata.get(which).getDsBranch());

                branch=branchdata.get(which);

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
