package com.pgpavers.admin.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.activities.InvoiceActivity;
import com.pgpavers.admin.activities.ProductionActivity;
import com.pgpavers.admin.activities.SalesPersonOutstandActivity;
import com.pgpavers.admin.adapters.BranchCustomerOutstandAdapter;
import com.pgpavers.admin.adapters.CustomerOutstandAdapter;
import com.pgpavers.admin.connectivity.NetConnection;
import com.pgpavers.admin.data.pojo.BranchCustomerOutstand;
import com.pgpavers.admin.data.pojo.OutStandBranch;
import com.pgpavers.admin.data.pojo.OutStandingCustomer;
import com.pgpavers.admin.progress.ProgressFragment;
import com.pgpavers.admin.webservice.RetrofitHelper;
import com.pgpavers.admin.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerFragment extends Fragment {

    TextView txtUser,txtNodata,txtSelectBranch;
    RecyclerView recycler_view;

    ProgressFragment progressFragment;

    Button btnSalesperson,btnBranch;
    LinearLayout layout_branch;

    View view;

    AppCompatImageView imgdropdownarrow;

    List<OutStandingCustomer>outStandingCustomers;
    List<OutStandBranch>outStandBranches;

    OutStandBranch outStandBranchselected;

    OutStandingCustomer outStandingCustomer_First;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.layout_customer,container,false);
        btnSalesperson=view.findViewById(R.id.btnSalesperson);

        btnBranch=view.findViewById(R.id.btnBranch);
        txtUser=view.findViewById(R.id.txtName);
        txtNodata=view.findViewById(R.id.txtNodata);
        imgdropdownarrow=view.findViewById(R.id.imgdropdownarrow);
        txtSelectBranch=view.findViewById(R.id.txtSelectBranch);
        layout_branch=view.findViewById(R.id.layout_branch);

        recycler_view=view.findViewById(R.id.recycler_view);
        outStandingCustomers=new ArrayList<>();
        outStandBranches=new ArrayList<>();
        showCustomerOutstand();



        btnBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btnBranch.setTextColor(Color.parseColor("#ffffff"));
                btnSalesperson.setTextColor(Color.parseColor("#000000"));
                btnBranch.setBackgroundResource(R.drawable.drawable_rightgreenbg);
                btnSalesperson.setBackgroundResource(R.drawable.drawable_leftbtnbg);
                layout_branch.setVisibility(View.VISIBLE);
                recycler_view.setVisibility(View.GONE);
                txtUser.setText("");
                outStandBranchselected=null;
                txtSelectBranch.setText("Select Branch");


                callBranchData(1);



            }
        });

        btnSalesperson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btnBranch.setTextColor(Color.parseColor("#000000"));
                btnSalesperson.setTextColor(Color.parseColor("#ffffff"));

                btnBranch.setBackgroundResource(R.drawable.drawable_rightbtnbg);
                btnSalesperson.setBackgroundResource(R.drawable.drawable_leftgreenbg);

                layout_branch.setVisibility(View.GONE);
                recycler_view.setVisibility(View.GONE);
                txtUser.setText("");


                showCustomerOutstand();

            }
        });

        txtSelectBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callBranchData(0);

            }
        });

        imgdropdownarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callBranchData(0);
            }
        });

        txtUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(outStandingCustomer_First!=null)
                {
                    Intent intent=new Intent(getActivity(), SalesPersonOutstandActivity.class);
                    intent.putExtra("Salesperson",outStandingCustomer_First);
                    intent.putExtra("Mainadmin",1);
                    startActivity(intent);
                }

            }
        });



        return view;
    }


    public void showCustomerOutstand()
    {


        outStandingCustomers=new ArrayList<>();

            progressFragment = new ProgressFragment();
            progressFragment.show(getFragmentManager(), "dfjk");

            WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(getActivity()).create(WebInterfaceimpl.class);


            Call<List<OutStandingCustomer>> listCall = webInterfaceimpl.customerOutstanding();


            listCall.enqueue(new Callback<List<OutStandingCustomer>>() {
                @Override
                public void onResponse(Call<List<OutStandingCustomer>> call, Response<List<OutStandingCustomer>> response) {

                    progressFragment.dismiss();

                    if (response.body() != null) {

                        if (response.body().size() > 0) {

                            outStandingCustomers.addAll(response.body());

                            if(outStandingCustomers.get(0).getMessage().equalsIgnoreCase("disable click"))
                            {
                                outStandingCustomer_First=outStandingCustomers.get(0);

                                txtUser.setText(outStandingCustomers.get(0).getNameSalesPerson()+"\n"+
                                        outStandingCustomers.get(0).getVlAdvance()+"\n"+
                                        outStandingCustomers.get(0).getVlBalance());

                                outStandingCustomers.remove(0);
                            }
                            else {
                                outStandingCustomer_First=null;
                                txtUser.setText("");
                            }

                            recycler_view.setVisibility(View.VISIBLE);
                            txtUser.setVisibility(View.VISIBLE);
                            txtNodata.setVisibility(View.GONE);


                            recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
                            recycler_view.setAdapter(new CustomerOutstandAdapter(getActivity(),outStandingCustomers,CustomerFragment.this));

                        }
                        else {

                            recycler_view.setVisibility(View.GONE);
                            txtNodata.setVisibility(View.VISIBLE);
                            txtUser.setVisibility(View.GONE);
                        }


                    }


                }

                @Override
                public void onFailure(Call<List<OutStandingCustomer>> call, Throwable t) {

                    progressFragment.dismiss();
                    if (!NetConnection.isConnected(getActivity())) {
                        Toast.makeText(getActivity(), "Check Internet connection", Toast.LENGTH_SHORT).show();
                    }

                }
            });



    }


    public void callBranchData(final int btnClick)
    {


        outStandBranches=new ArrayList<>();

            progressFragment = new ProgressFragment();
            progressFragment.show(getFragmentManager(), "dfjk");

            WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(getActivity()).create(WebInterfaceimpl.class);

            Call<List<OutStandBranch>>listCall=webInterfaceimpl.getBranchOutStand();

            listCall.enqueue(new Callback<List<OutStandBranch>>() {
                @Override
                public void onResponse(Call<List<OutStandBranch>> call, Response<List<OutStandBranch>> response) {
                    progressFragment.dismiss();


                    if(response.body()!=null)
                    {


                        if(response.body().size()>0)
                        {

                            outStandBranches.addAll(response.body());

                            recycler_view.setVisibility(View.GONE);
                            txtUser.setVisibility(View.VISIBLE);
                            txtNodata.setVisibility(View.GONE);

                            txtUser.setText("Total Advance : "+outStandBranches.get(0).getTotalAdvance()+"\n Total balance : "+outStandBranches.get(0).getTotalBalance());

                            outStandBranches.remove(0);

                            if(btnClick==0) {

                                showBranchAlert(outStandBranches);
                            }


                        }
                    }







                }

                @Override
                public void onFailure(Call<List<OutStandBranch>> call, Throwable t) {
                    progressFragment.dismiss();
                    if (!NetConnection.isConnected(getActivity())) {
                        Toast.makeText(getActivity(), "Check Internet connection", Toast.LENGTH_SHORT).show();
                    }
                }
            });



    }


    public void showBranchAlert(final List<OutStandBranch>outStandBranches)
    {


        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a Branch");
        for (OutStandBranch st:outStandBranches)
        {
            strings.add(st.getDsBranch());
        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                // pinCode=dist.get(which);
                outStandBranchselected=outStandBranches.get(which);

                txtSelectBranch.setText(outStandBranchselected.getDsBranch());

                showBranchCustomer(outStandBranchselected.getCdBranch());

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();



    }

    public void showBranchCustomer(String cd_branch)
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(getFragmentManager(), "dfjk");

        WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(getActivity()).create(WebInterfaceimpl.class);

        Call<List<BranchCustomerOutstand>>listCall=webInterfaceimpl.getBranchOutStandByID(cd_branch);
        listCall.enqueue(new Callback<List<BranchCustomerOutstand>>() {
            @Override
            public void onResponse(Call<List<BranchCustomerOutstand>> call, Response<List<BranchCustomerOutstand>> response) {
                progressFragment.dismiss();

                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {

                        recycler_view.setVisibility(View.VISIBLE);
                        txtUser.setVisibility(View.VISIBLE);
                        txtNodata.setVisibility(View.GONE);


                        recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recycler_view.setAdapter(new BranchCustomerOutstandAdapter(getActivity(),response.body(),CustomerFragment.this));







                    }
                    else {

                        recycler_view.setVisibility(View.GONE);

                        txtNodata.setVisibility(View.VISIBLE);
                    }





                }






            }

            @Override
            public void onFailure(Call<List<BranchCustomerOutstand>> call, Throwable t) {
                progressFragment.dismiss();
                if (!NetConnection.isConnected(getActivity())) {
                    Toast.makeText(getActivity(), "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });




    }


    public void getBranchViaInvoice(BranchCustomerOutstand branchCustomerOutstand)
    {
        if(branchCustomerOutstand!=null) {

            if(outStandBranchselected!=null) {
                Intent intent = new Intent(getActivity(), InvoiceActivity.class);
                intent.putExtra("branchcustomer", branchCustomerOutstand);
                intent.putExtra("branchid",outStandBranchselected.getCdBranch());
                startActivity(intent);
            }
        }
    }


    public void selectCustomerOutStand(OutStandingCustomer outStandingCustomer)
    {

        if(outStandingCustomer!=null)
        {

            Intent intent=new Intent(getActivity(), SalesPersonOutstandActivity.class);
            intent.putExtra("Salesperson",outStandingCustomer);
            startActivity(intent);




        }


    }
}
