package com.pgpavers.admin.fragments;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;

import com.pgpavers.admin.R;
import com.pgpavers.admin.activities.PurchaseActivity;
import com.pgpavers.admin.activities.SalesActivity;
import com.pgpavers.admin.connectivity.NetConnection;
import com.pgpavers.admin.data.pojo.DiscountSales;
import com.pgpavers.admin.data.pojo.SalesPerson;
import com.pgpavers.admin.progress.ProgressFragment;
import com.pgpavers.admin.webservice.RetrofitHelper;
import com.pgpavers.admin.webservice.WebInterfaceimpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DiscountSalesFragment extends Fragment {

    View view;

    String start_date = "", end_date = "";

    AppCompatImageView imgDropdownsales, imgpickFromDate, imgpickToDate;
    TextView txtSalesperson, txtStartdate, txttoDate,txtTotalCount;

    ProgressFragment progressFragment;

    SalesPerson salesPerson;

    Button btnSubmit;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.layout_discountsales,container,false);

        imgpickFromDate = view.findViewById(R.id.imgpickFromDate);
        imgpickToDate = view.findViewById(R.id.imgpickToDate);

        txtStartdate = view.findViewById(R.id.txtStartdate);
        txttoDate = view.findViewById(R.id.txttoDate);
        txtSalesperson=view.findViewById(R.id.txtSalesperson);
        imgDropdownsales=view.findViewById(R.id.imgDropdownsales);
        btnSubmit=view.findViewById(R.id.btnSubmit);
        txtTotalCount=view.findViewById(R.id.txtTotalCount);


        txtSalesperson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showAllSalesPersons();

            }
        });

        imgDropdownsales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showAllSalesPersons();

            }
        });


        imgpickToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);

            }
        });

        imgpickFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(1);

            }
        });

        txtStartdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);

            }
        });
        txttoDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);

            }
        });




        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(salesPerson!=null) {

                    if (!start_date.equals("")) {

                        if (!end_date.equals("")) {

                            Date sta_date = null;
                            Date end_dat = null;


                            try {

                                DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
                                sta_date = dateFormat.parse(start_date);
                                end_dat = dateFormat.parse(end_date);

                                if (sta_date.before(end_dat)) {




                                    showDiscountSales();



                                } else {
                                    Toast.makeText(getActivity(), "Select date properly", Toast.LENGTH_SHORT).show();

                                }


                            } catch (Exception e) {
                                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();

                            }


                        } else {
                            Toast.makeText(getActivity(), "select end date", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(getActivity(), "select start date", Toast.LENGTH_SHORT).show();
                    }
                }

                else {
                    Toast.makeText(getActivity(), "select a sales person", Toast.LENGTH_SHORT).show();
                }

            }
        });



        return view;
    }


    public void showDatePicker(final int start) {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                // txtDob.setText();

                int m = i1 + 1;

                if (start == 1) {
                    start_date = i + "-" + m + "-" + i2;
                    txtStartdate.setText(start_date);
                } else {
                    end_date = i + "-" + m + "-" + i2;
                    txttoDate.setText(end_date);
                }


            }
        }, year, month, day);

        datePickerDialog.show();
    }



    public void showDiscountSales()
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getFragmentManager(),"dfjk");

        WebInterfaceimpl webInterfaceimpl= RetrofitHelper.getRetrofitInstance(getActivity()).create(WebInterfaceimpl.class);
Call<List<DiscountSales>>listCall=webInterfaceimpl.getDiscountdetails(salesPerson.getCdSalesPerson(),start_date,end_date);

listCall.enqueue(new Callback<List<DiscountSales>>() {
    @Override
    public void onResponse(Call<List<DiscountSales>> call, Response<List<DiscountSales>> response) {

        progressFragment.dismiss();

        if(response.body()!=null)
        {

            if(response.body().size()>0)
            {

              txtTotalCount.setText("Total Discount : "+response.body().get(0).getTotalDiscount());




            }
            else {

                Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onFailure(Call<List<DiscountSales>> call, Throwable t) {
        progressFragment.dismiss();
        if(!NetConnection.isConnected(getActivity()))
        {
            Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
        }
    }
});

    }





    public void showAllSalesPersons()
    {
        progressFragment=new ProgressFragment();
        progressFragment.show(getFragmentManager(),"dfjk");

        WebInterfaceimpl webInterfaceimpl= RetrofitHelper.getRetrofitInstance(getActivity()).create(WebInterfaceimpl.class);
        Call<List<SalesPerson>> jsonObjectCall=webInterfaceimpl.getAllSalesPersons();
        jsonObjectCall.enqueue(new Callback<List<SalesPerson>>() {
            @Override
            public void onResponse(Call<List<SalesPerson>> call, Response<List<SalesPerson>> response) {

                progressFragment.dismiss();

                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {
                        List<SalesPerson>salesPeople=new ArrayList<>();
                        salesPeople.addAll(response.body());

                        SalesPerson salesPerson1=new SalesPerson();
                        salesPerson1.setCdSalesPerson("0");
                        salesPerson1.setDsSalesPerson("All sales persons");
                        salesPeople.add(0,salesPerson1);

                        showAllSalesPersonAlert(salesPeople);

                    }
                    else {

                        Toast.makeText(getActivity(),"No Sales person found",Toast.LENGTH_SHORT).show();
                    }
                }


            }

            @Override
            public void onFailure(Call<List<SalesPerson>> call, Throwable t) {
                progressFragment.dismiss();
                if(!NetConnection.isConnected(getActivity()))
                {
                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void showAllSalesPersonAlert(final List<SalesPerson>salesPeople)
    {
        final List<String>strings=new ArrayList<>();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a Sales Person");
        ;
        for (SalesPerson st:salesPeople)
        {
            strings.add(st.getDsSalesPerson());
        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtSalesperson.setText(salesPeople.get(which).getDsSalesPerson());

                salesPerson=salesPeople.get(which);

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
