package com.pgpavers.admin.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.adapters.BranchAdapter;
import com.pgpavers.admin.data.pojo.BranchListData;

public class StockBranchFragment extends DialogFragment {

    View view;
    RecyclerView recycler_view;
    BranchListData branchListData;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view=inflater.inflate(R.layout.layoutstockbranch,container,false);
        getDialog().setTitle("Stock Branch");

        recycler_view=view.findViewById(R.id.recycler_view);

        branchListData=(BranchListData) getArguments().getSerializable("data");

showStockBranch();

        return view;
    }


    public void showStockBranch()
    {
        recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycler_view.setAdapter(new BranchAdapter(getActivity(),branchListData.getBranchdata()));

    }

    @Override
    public void onResume() {
        super.onResume();

        Window window = getDialog().getWindow();
        if(window == null) return;
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = getActivity().getResources().getDisplayMetrics().widthPixels;;;

        window.setAttributes(params);


    }
}
