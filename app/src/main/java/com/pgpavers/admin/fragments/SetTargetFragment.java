package com.pgpavers.admin.fragments;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;

import com.google.gson.JsonObject;
import com.pgpavers.admin.R;
import com.pgpavers.admin.connectivity.NetConnection;
import com.pgpavers.admin.data.pojo.SalesPerson;
import com.pgpavers.admin.progress.ProgressFragment;
import com.pgpavers.admin.webservice.RetrofitHelper;
import com.pgpavers.admin.webservice.WebInterfaceimpl;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SetTargetFragment extends Fragment {


    TextView txtSales, txtStartdate, txtEnddate,txtTYpe;
    AppCompatImageView imgDropdownSales, imgpickFromDate, imgpickEndDate,imgDropdownType;
    EditText edtAmount;
    Button btnsetTarget;

    String start_date = "", end_date = "",type="";
    ProgressFragment progressFragment;
    View view;
    SalesPerson salesPerson;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.layout_settarget, container, false);
        edtAmount = view.findViewById(R.id.edtAmount);
        btnsetTarget = view.findViewById(R.id.btnsetTarget);

        imgDropdownSales = view.findViewById(R.id.imgDropdownSales);
        imgpickFromDate = view.findViewById(R.id.imgpickFromDate);
        txtSales = view.findViewById(R.id.txtSales);
        txtStartdate = view.findViewById(R.id.txtStartdate);
        imgpickEndDate = view.findViewById(R.id.imgpickEndDate);
        txtTYpe=view.findViewById(R.id.txtTYpe);
        imgDropdownType=view.findViewById(R.id.imgDropdownType);

        txtEnddate = view.findViewById(R.id.txtEnddate);

        txtTYpe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAllSalesPersonAlert();
            }
        });

        imgDropdownType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAllSalesPersonAlert();
            }
        });

        txtStartdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);
            }
        });

        imgpickFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(1);
            }
        });


        txtEnddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);
            }
        });

        imgpickEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(0);
            }
        });


        txtSales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showAllSalesPersons();
            }
        });

        imgDropdownSales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showAllSalesPersons();
            }
        });

        btnsetTarget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (salesPerson != null) {

                    if (!type.equals("")) {


                    if (!start_date.equals("")) {

                        if (!end_date.equals("")) {

                            Date sta_date = null;
                            Date end_dat = null;


                            try {

                                DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
                                sta_date = dateFormat.parse(start_date);
                                end_dat = dateFormat.parse(end_date);

                                if (sta_date.before(end_dat)) {

                                    if (!edtAmount.getText().toString().equals("")) {


                                        setTarget();


                                    } else {
                                        Toast.makeText(getActivity(), "enter amount", Toast.LENGTH_SHORT).show();
                                    }

                                }else {
                                    Toast.makeText(getActivity(), "select date properly", Toast.LENGTH_SHORT).show();
                                }

                            }catch (Exception e)
                            {

                            }

                        } else {
                            Toast.makeText(getActivity(), "select end date", Toast.LENGTH_SHORT).show();
                        }


                    } else {
                        Toast.makeText(getActivity(), "select start date", Toast.LENGTH_SHORT).show();
                    }

                    } else {
                        Toast.makeText(getActivity(), "select a target type", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(getActivity(), "select sales person", Toast.LENGTH_SHORT).show();
                }
            }
        });


        return view;
    }

    public void showAllSalesPersonAlert()
    {
//        final List<String>strings=new ArrayList<>();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a type");
        ;
//        for (SalesPerson st:salesPeople)
//        {
//            strings.add(st.getDsSalesPerson());
//        }

        final String arr[]={"sale","Collection"};


        builder.setItems(arr, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtTYpe.setText(arr[which]);

                int a=which+1;

                type=String.valueOf(a);

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showAllSalesPersons() {
        progressFragment = new ProgressFragment();
        progressFragment.show(getFragmentManager(), "dfjk");

        WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(getActivity()).create(WebInterfaceimpl.class);
        Call<List<SalesPerson>> jsonObjectCall = webInterfaceimpl.getAllSalesPersons();
        jsonObjectCall.enqueue(new Callback<List<SalesPerson>>() {
            @Override
            public void onResponse(Call<List<SalesPerson>> call, Response<List<SalesPerson>> response) {

                progressFragment.dismiss();

                if (response.body() != null) {

                    if (response.body().size() > 0) {

                        List<SalesPerson> salesPeople = new ArrayList<>();
                        salesPeople.addAll(response.body());

//                        SalesPerson salesPerson1=new SalesPerson();
//                        salesPerson1.setCdSalesPerson("0");
//                        salesPerson1.setDsSalesPerson("All sales persons");
//                        salesPeople.add(0,salesPerson1);

                        showAllSalesPersonAlert(salesPeople);

                    } else {

                        Toast.makeText(getActivity(), "No Sales person found", Toast.LENGTH_SHORT).show();
                    }
                }


            }

            @Override
            public void onFailure(Call<List<SalesPerson>> call, Throwable t) {
                progressFragment.dismiss();
                if (!NetConnection.isConnected(getActivity())) {
                    Toast.makeText(getActivity(), "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


//    public void showDatePicker() {
//        final Calendar cldr = Calendar.getInstance();
//        int day = cldr.get(Calendar.DAY_OF_MONTH);
//        int month = cldr.get(Calendar.MONTH);
//        int year = cldr.get(Calendar.YEAR);
//
//        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
//            @Override
//            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
//
//                // txtDob.setText();
//
//                int m = i1 + 1;
//
//
//                    start_date = i + "-" + m + "-" + i2;
//                    txtStartdate.setText(start_date);
//
//
//
//            }
//        }, year, month, day);
//
//        datePickerDialog.show();
//    }


    public void showDatePicker(final int start) {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                // txtDob.setText();

                int m = i1 + 1;

                if (start == 1) {
                    start_date = i + "-" + m + "-" + i2;
                    txtStartdate.setText(start_date);
                } else {
                    end_date = i + "-" + m + "-" + i2;
                    txtEnddate.setText(end_date);
                }


            }
        }, year, month, day);

        datePickerDialog.show();
    }

    public void showAllSalesPersonAlert(final List<SalesPerson> salesPeople) {
        final List<String> strings = new ArrayList<>();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a Sales Person");
        ;
        for (SalesPerson st : salesPeople) {
            strings.add(st.getDsSalesPerson());
        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtSales.setText(salesPeople.get(which).getDsSalesPerson());

                salesPerson = salesPeople.get(which);

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void setTarget() {

        progressFragment = new ProgressFragment();
        progressFragment.show(getFragmentManager(), "dfjk");

        WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(getActivity()).create(WebInterfaceimpl.class);

        Call<JsonObject> jsonObjectCall = webInterfaceimpl.setTarget(type,salesPerson.getCdSalesPerson(), edtAmount.getText().toString(), start_date,end_date);
        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressFragment.dismiss();

                try {

                    JSONObject jsonObject = new JSONObject(response.body().toString());

                    Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();


                } catch (Exception e) {

                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressFragment.dismiss();
                if (!NetConnection.isConnected(getActivity())) {
                    Toast.makeText(getActivity(), "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
