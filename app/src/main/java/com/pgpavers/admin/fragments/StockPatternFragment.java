package com.pgpavers.admin.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.activities.StockActivity;
import com.pgpavers.admin.adapters.PatternAdapter;
import com.pgpavers.admin.data.pojo.Pattern;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class StockPatternFragment extends DialogFragment {

    View view;

    List<Pattern>patterns,patternList;

    EditText edtSearch;
    RecyclerView recycler_view;

    Pattern pattern_selected=null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view=inflater.inflate(R.layout.layout_stockpattern,container,false);
        edtSearch=view.findViewById(R.id.edtSearch);
        recycler_view=view.findViewById(R.id.recycler_view);
        setPatternAdapter();

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!charSequence.toString().equals(""))
                {
                    patternList=new ArrayList<>();

                    for (Pattern pattern:patterns)
                    {
                        if(pattern.getDsPattern().contains(charSequence.toString().toLowerCase(Locale.getDefault()))||pattern.getDsPattern().toUpperCase(Locale.getDefault()).contains(charSequence.toString().toUpperCase(Locale.getDefault())))
                        {
                            patternList.add(pattern);

                        }
                    }

                    recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recycler_view.setAdapter(new PatternAdapter(getActivity(),patternList,StockPatternFragment.this));
                }
                else {

                    setPatternAdapter();
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return view;
    }

    public void setPatterns(List<Pattern>patterns)
    {
        this.patterns=patterns;
    }

    public void setPatternAdapter()
    {
        recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycler_view.setAdapter(new PatternAdapter(getActivity(),patterns,StockPatternFragment.this));
    }


    public void setSelectedPattern(Pattern pattern_selected)
    {
        dismiss();
        this.pattern_selected=pattern_selected;

        ((StockActivity)getActivity()).callStock(pattern_selected);
    }


    @Override
    public void onResume() {
        super.onResume();

        Window window = getDialog().getWindow();
        if(window == null) return;
        WindowManager.LayoutParams params = window.getAttributes();
        params.width =  getActivity().getResources().getDisplayMetrics().widthPixels;;

        window.setAttributes(params);
    }
}
