package com.pgpavers.admin.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.activities.ProductiondetailsActivity;
import com.pgpavers.admin.activities.StockActivity;
import com.pgpavers.admin.adapters.PatternAdapter;
import com.pgpavers.admin.adapters.ProductAdapter;
import com.pgpavers.admin.data.pojo.Pattern;
import com.pgpavers.admin.data.pojo.Products;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ProductFragment extends DialogFragment {

    View view;



    EditText edtSearch;
    RecyclerView recycler_view;

    List<Products>products,productsList;

    Products prod;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.layout_stockpattern,container,false);
        edtSearch=view.findViewById(R.id.edtSearch);
        recycler_view=view.findViewById(R.id.recycler_view);

        setupProductAdapter();

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(!charSequence.toString().equals(""))
                {
                    productsList=new ArrayList<>();

                    for (Products pattern:products)
                    {
                        if(pattern.getDsProduct().contains(charSequence.toString().toLowerCase(Locale.getDefault()))||pattern.getDsProduct().toUpperCase(Locale.getDefault()).contains(charSequence.toString().toUpperCase(Locale.getDefault())))
                        {
                            productsList.add(pattern);

                        }
                    }

                    recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recycler_view.setAdapter(new ProductAdapter(getActivity(),productsList,ProductFragment.this));





                }
                else {

                    recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recycler_view.setAdapter(new ProductAdapter(getActivity(),products,ProductFragment.this));





                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        return view;
    }

    public void setProducts(List<Products>products)
    {

        this.products=products;


    }

    public void setupProductAdapter()
    {
        ProductAdapter productAdapter=new ProductAdapter(getActivity(),products,ProductFragment.this);
        recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycler_view.setAdapter(productAdapter);
    }

    public void setSelectedProducts(Products pattern_selected)
    {
        dismiss();
        this.prod=pattern_selected;

        ((ProductiondetailsActivity)getActivity()).getProductwiseDetails(pattern_selected);
    }

    @Override
    public void onResume() {
        super.onResume();

        Window window = getDialog().getWindow();
        if(window == null) return;
        WindowManager.LayoutParams params = window.getAttributes();
        params.width =  getActivity().getResources().getDisplayMetrics().widthPixels;;

        window.setAttributes(params);
    }
}
