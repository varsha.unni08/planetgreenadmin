package com.pgpavers.admin.adapters;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.pgpavers.admin.fragments.CustomerFragment;
import com.pgpavers.admin.fragments.SupplierFragment;

public class TabPagerAdapter extends FragmentPagerAdapter {


    String arr[]={"Customer","Supplier"};



    public TabPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment=null;

        if(position==0)
        {
           fragment=new CustomerFragment();
        }
        else if(position==1)
        {
            fragment=new SupplierFragment();
        }


        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return arr[position];
    }
}
