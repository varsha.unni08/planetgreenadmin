package com.pgpavers.admin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.data.pojo.InVoice;

import java.util.List;

public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.InvoiceHolder> {


    Context context;
    List<InVoice>inVoices;

    public InvoiceAdapter(Context context, List<InVoice> inVoices) {
        this.context = context;
        this.inVoices = inVoices;
    }

    public class InvoiceHolder extends RecyclerView.ViewHolder
    {
        TextView txtdetails;

        public InvoiceHolder(@NonNull View itemView) {
            super(itemView);
            txtdetails=itemView.findViewById(R.id.txtdetails);
        }
    }


    @NonNull
    @Override
    public InvoiceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_productionadapter,parent,false);


        return new InvoiceHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InvoiceHolder holder, int position) {

        holder.txtdetails.setText("Invoice no. : "+inVoices.get(position).getDsSalesInvoice()+"\n"+
                "Sale date : "+inVoices.get(position).getDtProductSale()+"\n"+
                "Balance : "+inVoices.get(position).getVlBalance()+"\n"+
                "Grand total : "+inVoices.get(position).getVlGrandTotal()+"\n"+
                "Ageing : "+inVoices.get(position).getAgeing());

    }

    @Override
    public int getItemCount() {
        return inVoices.size();
    }
}
