package com.pgpavers.admin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.activities.StockActivity;
import com.pgpavers.admin.data.pojo.StockData;

import java.util.List;

public class StockAdapter extends RecyclerView.Adapter<StockAdapter.StockHolder> {

    Context context;
    List<StockData>stockData;

    public StockAdapter(Context context, List<StockData> stockData) {
        this.context = context;
        this.stockData = stockData;
    }

    public class StockHolder extends RecyclerView.ViewHolder{

        TextView txtPattern;

        public StockHolder(@NonNull View itemView) {
            super(itemView);
            txtPattern=itemView.findViewById(R.id.txtPattern);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ((StockActivity)context).showStockBranch(stockData.get(getAdapterPosition()).getCdProduct());
                }
            });

        }
    }

    @NonNull
    @Override
    public StockHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new StockHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_stockadapter,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull StockHolder holder, int position) {
        holder.txtPattern.setText(stockData.get(position).getDsProduct()+"\n Qty : "+stockData.get(position).getNrQty());

    }

    @Override
    public int getItemCount() {
        return stockData.size();
    }
}
