package com.pgpavers.admin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.data.pojo.SalesCollection;

import java.util.List;

public class SalesCollectionAdapter extends RecyclerView.Adapter<SalesCollectionAdapter.SalesCollectionholder> {


    Context context;
    List<SalesCollection>salesCollections;

    public SalesCollectionAdapter(Context context, List<SalesCollection> salesCollections) {
        this.context = context;
        this.salesCollections = salesCollections;
    }

    public class SalesCollectionholder extends RecyclerView.ViewHolder{

        TextView txtdetails;

        public SalesCollectionholder(@NonNull View itemView) {
            super(itemView);

            txtdetails=itemView.findViewById(R.id.txtdetails);
        }
    }

    @NonNull
    @Override
    public SalesCollectionholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_productionadapter,parent,false);


        return new SalesCollectionholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SalesCollectionholder holder, int position) {

        holder.txtdetails.setText("Customer : "+salesCollections.get(position).getDsCustomer()+"\nBranch code : "+salesCollections.get(position).getDsBranchCode()+"\nSales person : "+salesCollections.get(position).getDsSalesPerson()+"\n Recorded date : "+salesCollections.get(position).getDtRecord()+"\nAmount : "+salesCollections.get(position).getVlAmount());

    }

    @Override
    public int getItemCount() {
        return salesCollections.size();
    }
}
