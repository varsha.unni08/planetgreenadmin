package com.pgpavers.admin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.data.pojo.DiscountSales;

import java.util.List;

public class DiscountSalesAdapter extends RecyclerView.Adapter<DiscountSalesAdapter.DiscountHolder> {


    Context context;
    List<DiscountSales>discountSales;

    public DiscountSalesAdapter(Context context, List<DiscountSales> discountSales) {
        this.context = context;
        this.discountSales = discountSales;
    }

    public class DiscountHolder extends RecyclerView.ViewHolder{

        public DiscountHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    @NonNull
    @Override
    public DiscountHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_productionadapter,parent,false);


        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull DiscountHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return discountSales.size();
    }
}
