package com.pgpavers.admin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.activities.ProductionActivity;
import com.pgpavers.admin.data.pojo.Productiondata;

import java.util.List;

public class ProductionAdapter extends RecyclerView.Adapter<ProductionAdapter.Productionholder> {


    Context context;
    List<Productiondata>productiondata;

    public ProductionAdapter(Context context, List<Productiondata> productiondata) {
        this.context = context;
        this.productiondata = productiondata;
    }

    public class Productionholder extends RecyclerView.ViewHolder
    {

        TextView txtdetails;

        public Productionholder(@NonNull View itemView) {
            super(itemView);

            txtdetails=itemView.findViewById(R.id.txtdetails);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ((ProductionActivity)context).getSelectedData(productiondata.get(getAdapterPosition()));



                }
            });
        }
    }

    @NonNull
    @Override
    public Productionholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_productionadapter,parent,false);


        return new Productionholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Productionholder holder, int position) {

        holder.txtdetails.setText("Branch : "+productiondata.get(position).getDsBranch()+"\n"+
                "Branch code : "+productiondata.get(position).getDsBranchCode()+"\n"+
                "Total produced qty : "+productiondata.get(position).getBranchwiseProducedQty());

    }

    @Override
    public int getItemCount() {
        return productiondata.size();
    }
}
