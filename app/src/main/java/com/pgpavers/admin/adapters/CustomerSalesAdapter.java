package com.pgpavers.admin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.data.pojo.CustomerOutstand;
import com.pgpavers.admin.fragments.SalesPersonFragment;

import java.util.List;

public class CustomerSalesAdapter extends RecyclerView.Adapter<CustomerSalesAdapter.CustomerHolder> {

    Context context;
    List<CustomerOutstand> customerList;
    SalesPersonFragment salesPersonFragment;

    public CustomerSalesAdapter(Context context, List<CustomerOutstand> customerList, SalesPersonFragment salesPersonFragment) {
        this.context = context;
        this.customerList = customerList;
        this.salesPersonFragment=salesPersonFragment;
    }

    public class CustomerHolder extends RecyclerView.ViewHolder{

        TextView txtdetails;

        public CustomerHolder(@NonNull View itemView) {
            super(itemView);

            txtdetails=itemView.findViewById(R.id.txtdetails);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    salesPersonFragment.getCustomerOutstand(customerList.get(getAdapterPosition()));
                }
            });
        }
    }


    @NonNull
    @Override
    public CustomerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_productionadapter,parent,false);

        return new CustomerHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomerHolder holder, int position) {

//        if(position==0) {
//
//            holder.txtdetails.setText(customerList.get(position).getCustomerOutstandingUnderSalesperson());
//        }
//        else {

            holder.txtdetails.setText(customerList.get(position).getDsCustomer()+"\n"+customerList.get(position).getVlBalance()+"\n"+customerList.get(position).getVlAdvance());
       // }

    }

    @Override
    public int getItemCount() {
        return customerList.size();
    }
}
