package com.pgpavers.admin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.activities.OrdersActivity;
import com.pgpavers.admin.connectivity.NetConnection;
import com.pgpavers.admin.data.pojo.AdminOrders;
import com.pgpavers.admin.data.pojo.StatusMsg;
import com.pgpavers.admin.progress.ProgressFragment;
import com.pgpavers.admin.webservice.RetrofitHelper;
import com.pgpavers.admin.webservice.WebInterfaceimpl;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminOrderAdapter extends RecyclerView.Adapter<AdminOrderAdapter.AdminOrderHolder> {

    Context context;
    List<AdminOrders>adminOrders;

    ProgressFragment progressFragment;

    public AdminOrderAdapter(Context context, List<AdminOrders> adminOrders) {
        this.context = context;
        this.adminOrders = adminOrders;
    }

    public class AdminOrderHolder extends RecyclerView.ViewHolder{

        TextView txtData,txtapprove,txtDelete;
        LinearLayout layout_action;
        ImageView imgApprove,imgDelete;

        public AdminOrderHolder(@NonNull View itemView) {
            super(itemView);
            txtData=itemView.findViewById(R.id.txtData);
            layout_action=itemView.findViewById(R.id.layout_action);
            txtapprove=itemView.findViewById(R.id.txtapprove);
            txtDelete=itemView.findViewById(R.id.txtDelete);
            imgDelete=itemView.findViewById(R.id.imgDelete);
            imgApprove=itemView.findViewById(R.id.imgApprove);
            imgApprove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    approveAdminOrder(adminOrders.get(getAdapterPosition()),getAdapterPosition());
                }
            });

            txtapprove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    approveAdminOrder(adminOrders.get(getAdapterPosition()),getAdapterPosition());
                }
            });

            txtDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    deleteOrder(adminOrders.get(getAdapterPosition()),getAdapterPosition());

                }
            });

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    deleteOrder(adminOrders.get(getAdapterPosition()),getAdapterPosition());
                }
            });



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(adminOrders.get(getAdapterPosition()).getSeolected()==1)
                    {
                        for (AdminOrders adminOrders:adminOrders
                             ) {

                            adminOrders.setSeolected(0);

                        }
                        notifyItemChanged(getAdapterPosition());
                    }
                    else {

                        for (AdminOrders adminOrders:adminOrders
                        ) {

                            adminOrders.setSeolected(0);

                        }

                        adminOrders.get(getAdapterPosition()).setSeolected(1);

                        notifyItemRangeChanged(0,adminOrders.size());


                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public AdminOrderHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_adminorderadapter,parent,false);


        return new AdminOrderHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdminOrderHolder holder, int position) {
        holder.txtData.setText(adminOrders.get(position).getDsCustomer()+"\n"+adminOrders.get(position).getDtDelivery()+"\n"+adminOrders.get(position).getVlTotal()+" Rs");

        if(adminOrders.get(position).getSeolected()==1)
        {
            holder.layout_action.setVisibility(View.VISIBLE);
        }
        else {
            holder.layout_action.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return adminOrders.size();
    }


    public void deleteOrder(final AdminOrders adminOrder, final int position)
    {
        progressFragment=new ProgressFragment();
        progressFragment.show(((AppCompatActivity)context).getSupportFragmentManager(),"dfjk");

        WebInterfaceimpl webInterfaceimpl= RetrofitHelper.getRetrofitInstance(context).create(WebInterfaceimpl.class);
        Call<List<StatusMsg>>listCall=webInterfaceimpl.deleteSalesOrder(adminOrder.getCdSalesOrder());

        listCall.enqueue(new Callback<List<StatusMsg>>() {
            @Override
            public void onResponse(Call<List<StatusMsg>> call, Response<List<StatusMsg>> response) {
                progressFragment.dismiss();

                if(response.body()!=null)
                {

                    if(response.body().get(0)!=null)
                    {


                        Toast.makeText(context,response.body().get(0).getMessage(),Toast.LENGTH_SHORT).show();

                       // ((OrdersActivity)context).getAllOrders();

                        adminOrders.remove(position);
                        notifyItemRemoved(position);

                    }
                    else {

                        Toast.makeText(context,"Sorry, we cannot approve the order",Toast.LENGTH_SHORT).show();

                    }




                }
                else {

                    Toast.makeText(context,"Sorry, we cannot approve the order",Toast.LENGTH_SHORT).show();

                }




            }

            @Override
            public void onFailure(Call<List<StatusMsg>> call, Throwable t) {
                progressFragment.dismiss();

                if(!NetConnection.isConnected(context))
                {
                    Toast.makeText(context,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }




    public void approveAdminOrder(AdminOrders adminOrder,final int position)
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(((AppCompatActivity)context).getSupportFragmentManager(),"dfjk");

        WebInterfaceimpl webInterfaceimpl= RetrofitHelper.getRetrofitInstance(context).create(WebInterfaceimpl.class);

        Call<List<StatusMsg>>listCall=webInterfaceimpl.approveSalesOrder(adminOrder.getCdSalesOrder());

        listCall.enqueue(new Callback<List<StatusMsg>>() {
            @Override
            public void onResponse(Call<List<StatusMsg>> call, Response<List<StatusMsg>> response) {
                progressFragment.dismiss();

                if(response.body()!=null)
                {

                    if(response.body().get(0)!=null)
                    {


                        Toast.makeText(context,response.body().get(0).getMessage(),Toast.LENGTH_SHORT).show();
                        adminOrders.remove(position);
                        notifyItemRemoved(position);

                    }
                    else {

                        Toast.makeText(context,"Sorry, we cannot approve the order",Toast.LENGTH_SHORT).show();

                    }




                }
                else {

                    Toast.makeText(context,"Sorry, we cannot approve the order",Toast.LENGTH_SHORT).show();

                }




            }

            @Override
            public void onFailure(Call<List<StatusMsg>> call, Throwable t) {
                progressFragment.dismiss();

                if(!NetConnection.isConnected(context))
                {
                    Toast.makeText(context,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });




    }
}
