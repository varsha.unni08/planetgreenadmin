package com.pgpavers.admin.adapters;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.pgpavers.admin.fragments.CustomerFragment;
import com.pgpavers.admin.fragments.DiscountBranchFragment;
import com.pgpavers.admin.fragments.DiscountSalesFragment;
import com.pgpavers.admin.fragments.SupplierFragment;

public class DiscountTabPagerAdapter extends FragmentPagerAdapter {

    String arr[]={"Sales person","Branch"};

    public DiscountTabPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment=null;

        if(position==0)
        {
            fragment=new DiscountSalesFragment();
        }
        else if(position==1)
        {
            fragment=new DiscountBranchFragment();
        }


        return fragment;
    }

    @Override
    public int getCount() {
        return arr.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return arr[position];
    }
}
