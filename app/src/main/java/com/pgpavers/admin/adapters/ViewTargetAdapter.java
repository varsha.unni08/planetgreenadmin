package com.pgpavers.admin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.data.pojo.ViewTargetDate;

import java.util.List;

public class ViewTargetAdapter extends RecyclerView.Adapter<ViewTargetAdapter.ViewTargetHolder> {


    Context context;
    List<ViewTargetDate>viewTargets;

    public ViewTargetAdapter(Context context, List<ViewTargetDate> viewTargets) {
        this.context = context;
        this.viewTargets = viewTargets;
    }

    public class ViewTargetHolder extends RecyclerView.ViewHolder{

        TextView txtdetails;

        public ViewTargetHolder(@NonNull View itemView) {
            super(itemView);

            txtdetails=itemView.findViewById(R.id.txtdetails);
        }
    }


    @NonNull
    @Override
    public ViewTargetHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_productionadapter,parent,false);


        return new ViewTargetHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewTargetHolder holder, int position) {

        holder.txtdetails.setText(viewTargets.get(position).getDsSalesPerson()+"\nTarget : "+viewTargets.get(position).getTarget()+"\nAcheivement : "+viewTargets.get(position).getAchievement());

    }

    @Override
    public int getItemCount() {
        return viewTargets.size();
    }
}
