package com.pgpavers.admin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.activities.SalesPersonOutstandActivity;
import com.pgpavers.admin.data.pojo.SalesPersonOutStanding;

import java.util.List;

public class SalesPersonOutStandAdapter extends RecyclerView.Adapter<SalesPersonOutStandAdapter.SalesPersonHolder> {


    Context context;
    List<SalesPersonOutStanding>salesPersonOutStandings;

    public SalesPersonOutStandAdapter(Context context, List<SalesPersonOutStanding> salesPersonOutStandings) {
        this.context = context;
        this.salesPersonOutStandings = salesPersonOutStandings;
    }

    public class SalesPersonHolder extends RecyclerView.ViewHolder
    {

        TextView txtdetails;

        public SalesPersonHolder(@NonNull View itemView) {
            super(itemView);
            txtdetails=itemView.findViewById(R.id.txtdetails);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((SalesPersonOutstandActivity)context).getSalesPersonOutstand(salesPersonOutStandings.get(getAdapterPosition()));
                }
            });
        }
    }


    @NonNull
    @Override
    public SalesPersonHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_productionadapter,parent,false);

        return new SalesPersonHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SalesPersonHolder holder, int position) {

        holder.txtdetails.setText(salesPersonOutStandings.get(position).getCustomerOutstandingUnderSalesperson()+"\n"+salesPersonOutStandings.get(position).getVlAdvance()+"\n"+salesPersonOutStandings.get(position).getVlBalance());

    }

    @Override
    public int getItemCount() {
        return salesPersonOutStandings.size();
    }
}
