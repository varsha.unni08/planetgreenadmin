package com.pgpavers.admin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.activities.ProductionActivity;
import com.pgpavers.admin.data.pojo.Products;
import com.pgpavers.admin.fragments.ProductFragment;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductHolder> {


    Context context;
    List<Products>products;
    ProductFragment productFragment;

    public ProductAdapter(Context context, List<Products> products,ProductFragment productFragment) {
        this.context = context;
        this.products = products;
        this.productFragment=productFragment;
    }

    public class ProductHolder extends RecyclerView.ViewHolder{

        TextView txtdetails;


        public ProductHolder(@NonNull View itemView) {
            super(itemView);

            txtdetails=itemView.findViewById(R.id.txtdetails);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   // ((ProductionActivity)context).getSelectedData(productiondata.get(getAdapterPosition()));


                    productFragment.setSelectedProducts(products.get(getAdapterPosition()));


                }
            });
        }
    }

    @NonNull
    @Override
    public ProductHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_productionadapter,parent,false);


        return new ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductHolder holder, int position) {

        holder.txtdetails.setText(products.get(position).getDsProduct());

    }

    @Override
    public int getItemCount() {
        return products.size();
    }
}
