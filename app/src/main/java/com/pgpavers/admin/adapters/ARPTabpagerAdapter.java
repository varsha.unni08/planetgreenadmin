package com.pgpavers.admin.adapters;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.pgpavers.admin.fragments.ArpBranchFragment;
import com.pgpavers.admin.fragments.ArpSalesFragment;
import com.pgpavers.admin.fragments.DiscountBranchFragment;
import com.pgpavers.admin.fragments.DiscountSalesFragment;

public class ARPTabpagerAdapter extends FragmentPagerAdapter {


    String arr[]={"Sales person","Branch"};


    public ARPTabpagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment=null;

        if(position==0)
        {
            fragment=new ArpSalesFragment();
        }
        else if(position==1)
        {
            fragment=new ArpBranchFragment();
        }


        return fragment;
    }

    @Override
    public int getCount() {
        return arr.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return arr[position];
    }
}
