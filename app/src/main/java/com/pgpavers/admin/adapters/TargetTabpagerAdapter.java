package com.pgpavers.admin.adapters;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.pgpavers.admin.fragments.SetTargetFragment;
import com.pgpavers.admin.fragments.ViewTargetFragment;

public class TargetTabpagerAdapter extends FragmentPagerAdapter {

    String arr[]={"Set Target","View Target"};

    public TargetTabpagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment=null;

        if(position==0)
        {
            fragment=new SetTargetFragment();
        }
        else {

            fragment=new ViewTargetFragment();
        }



        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }


    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return arr[position];
    }
}
