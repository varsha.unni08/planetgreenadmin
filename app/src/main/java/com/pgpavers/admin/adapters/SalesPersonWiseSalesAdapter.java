package com.pgpavers.admin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.data.pojo.SalesPersonWiseSales;

import java.util.List;

public class SalesPersonWiseSalesAdapter extends RecyclerView.Adapter<SalesPersonWiseSalesAdapter.SalesPersonWiseSalesHolder> {

    Context context;
    List<SalesPersonWiseSales>salesPersonWiseSales;

    public SalesPersonWiseSalesAdapter(Context context, List<SalesPersonWiseSales> salesPersonWiseSales) {
        this.context = context;
        this.salesPersonWiseSales = salesPersonWiseSales;
    }

    public class SalesPersonWiseSalesHolder extends RecyclerView.ViewHolder{

        TextView txtdetails;

        public SalesPersonWiseSalesHolder(@NonNull View itemView) {
            super(itemView);
            txtdetails=itemView.findViewById(R.id.txtdetails);
        }
    }

    @NonNull
    @Override
    public SalesPersonWiseSalesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_productionadapter,parent,false);

        return new SalesPersonWiseSalesHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SalesPersonWiseSalesHolder holder, int position) {

        holder.txtdetails.setText("Customer : "+salesPersonWiseSales.get(position).getDsCustomer()+"\n Branch code : "+salesPersonWiseSales.get(position).getDsBranchCode()+"\n Invoice : "+salesPersonWiseSales.get(position).getDsSalesInvoice()+"\n Date of sale : "+salesPersonWiseSales.get(position).getDtProductSale()+"\n Grand Total : "+salesPersonWiseSales.get(position).getVlGrandTotal());

    }

    @Override
    public int getItemCount() {
        return salesPersonWiseSales.size();
    }
}
