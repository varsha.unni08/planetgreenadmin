package com.pgpavers.admin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.activities.SalesPersonOutstandActivity;
import com.pgpavers.admin.data.pojo.BranchCustomerOutstand;
import com.pgpavers.admin.fragments.CustomerFragment;

import java.util.List;

public class BranchCustomerOutstandAdapter extends RecyclerView.Adapter<BranchCustomerOutstandAdapter.BranchCustomerOutHolder> {


    Context context;
    List<BranchCustomerOutstand>branchCustomerOutstands;
    CustomerFragment customerFragment;

    public BranchCustomerOutstandAdapter(Context context, List<BranchCustomerOutstand> branchCustomerOutstands, CustomerFragment customerFragment) {
        this.context = context;
        this.branchCustomerOutstands = branchCustomerOutstands;
        this.customerFragment=customerFragment;
    }

    public class BranchCustomerOutHolder extends RecyclerView.ViewHolder{

        TextView txtdetails;

        public BranchCustomerOutHolder(@NonNull View itemView) {
            super(itemView);

            txtdetails=itemView.findViewById(R.id.txtdetails);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    customerFragment.getBranchViaInvoice(branchCustomerOutstands.get(getAdapterPosition()));

                }
            });


        }
    }


    @NonNull
    @Override
    public BranchCustomerOutHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_productionadapter,parent,false);

        return new BranchCustomerOutHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BranchCustomerOutHolder holder, int position) {

        holder.txtdetails.setText(branchCustomerOutstands.get(position).getDsCustomer()+"\n"+branchCustomerOutstands.get(position).getVlAdvance()+"\n"+branchCustomerOutstands.get(position).getVlBalance());

    }

    @Override
    public int getItemCount() {
        return branchCustomerOutstands.size();
    }
}
