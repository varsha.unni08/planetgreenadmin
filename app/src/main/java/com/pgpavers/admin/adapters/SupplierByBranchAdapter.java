package com.pgpavers.admin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.data.pojo.SupplierByBranch;

import java.util.List;

public class SupplierByBranchAdapter extends RecyclerView.Adapter<SupplierByBranchAdapter.SupplierHolder> {


    Context context;
    List<SupplierByBranch>supplierByBranches;

    public SupplierByBranchAdapter(Context context, List<SupplierByBranch> supplierByBranches) {
        this.context = context;
        this.supplierByBranches = supplierByBranches;
    }

    public class SupplierHolder extends RecyclerView.ViewHolder{

        TextView txtdetails;

        public SupplierHolder(@NonNull View itemView) {
            super(itemView);

            txtdetails=itemView.findViewById(R.id.txtdetails);
        }
    }

    @NonNull
    @Override
    public SupplierHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_productionadapter,parent,false);




        return new SupplierHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SupplierHolder holder, int position) {

        holder.txtdetails.setText("Supplier : "+supplierByBranches.get(position).getDsSupplier()+"\nTotal advance : "+supplierByBranches.get(position).getTotalAdvance()+"\nTotal balance : "+supplierByBranches.get(position).getTotalBalance());

    }

    @Override
    public int getItemCount() {
        return supplierByBranches.size();
    }
}
