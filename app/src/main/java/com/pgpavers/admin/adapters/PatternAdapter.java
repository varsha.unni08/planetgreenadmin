package com.pgpavers.admin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.data.pojo.Pattern;
import com.pgpavers.admin.fragments.StockPatternFragment;

import java.util.List;

public class PatternAdapter extends RecyclerView.Adapter<PatternAdapter.PatternHolder> {


    Context context;
    List<Pattern>patterns;
    StockPatternFragment stockPatternFragment;

    public PatternAdapter(Context context, List<Pattern> patterns, StockPatternFragment stockPatternFragment) {
        this.context = context;
        this.patterns = patterns;
        this.stockPatternFragment=stockPatternFragment;
    }

    public  class PatternHolder extends RecyclerView.ViewHolder{

        TextView txtPattern;

        public PatternHolder(@NonNull View itemView) {
            super(itemView);
            txtPattern=itemView.findViewById(R.id.txtPattern);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    stockPatternFragment.setSelectedPattern(patterns.get(getAdapterPosition()));
                }
            });
        }
    }

    @NonNull
    @Override
    public PatternHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_stockadapter,parent,false);


        return new PatternHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PatternHolder holder, int position) {
        holder.txtPattern.setText(patterns.get(position).getDsPattern());

    }

    @Override
    public int getItemCount() {
        return patterns.size();
    }
}
