package com.pgpavers.admin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.data.pojo.SalesByBranch;

import java.util.List;

public class SalesByBranchAdapter extends RecyclerView.Adapter<SalesByBranchAdapter.SalesByBranchHolder> {


    Context context;
    List<SalesByBranch>salesByBranches;

    public SalesByBranchAdapter(Context context, List<SalesByBranch> salesByBranches) {
        this.context = context;
        this.salesByBranches = salesByBranches;
    }

    public class SalesByBranchHolder extends RecyclerView.ViewHolder{

        TextView txtdetails;

        public SalesByBranchHolder(@NonNull View itemView) {
            super(itemView);

            txtdetails=itemView.findViewById(R.id.txtdetails);
        }
    }

    @NonNull
    @Override
    public SalesByBranchHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_productionadapter,parent,false);


        return new SalesByBranchHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SalesByBranchHolder holder, int position) {

        holder.txtdetails.setText("Sales Type : "+salesByBranches.get(position).getSalesType()+"\nSeries : "+salesByBranches.get(position).getSeries()
        +"\n Customer : "+salesByBranches.get(position).getDsCustomer()+"\n Sales invoice : "+salesByBranches.get(position).getDsSalesInvoice()+"\nDate of product sale : "+salesByBranches.get(position).getDtProductSale()+"\nGrand total : "+salesByBranches.get(position).getVlGrandTotal());

    }

    @Override
    public int getItemCount() {
        return salesByBranches.size();
    }
}
