package com.pgpavers.admin.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.activities.CollectionsActivity;
import com.pgpavers.admin.activities.DiscountsActivity;
import com.pgpavers.admin.activities.OrdersActivity;
import com.pgpavers.admin.activities.OutstandingActivity;
import com.pgpavers.admin.activities.ProductionActivity;
import com.pgpavers.admin.activities.PurchaseActivity;
import com.pgpavers.admin.activities.SalesActivity;
import com.pgpavers.admin.activities.StockActivity;
import com.pgpavers.admin.activities.TargetActivity;
import com.pgpavers.admin.data.appconstants.Literals;

public class Homeadapter extends RecyclerView.Adapter<Homeadapter.HomeHolder> {

    Context context;
    String arr[];

    public Homeadapter(Context context, String[] arr) {
        this.context = context;
        this.arr = arr;
    }

    public class HomeHolder extends RecyclerView.ViewHolder
    {

        TextView txtdata;
        ImageView img;
        public HomeHolder(@NonNull View itemView) {
            super(itemView);
            txtdata=itemView.findViewById(R.id.txtdata);
            img=itemView.findViewById(R.id.img);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    switch (getAdapterPosition())
                    {

                        case 0:
                            context.startActivity(new Intent(context, OrdersActivity.class));
                            break;
                        case 1:

                            context.startActivity(new Intent(context, SalesActivity.class));
                            break;
                        case 2:

                            context.startActivity(new Intent(context, CollectionsActivity.class));
                            break;
                        case 3:
                            context.startActivity(new Intent(context, OutstandingActivity.class));
                            break;
                        case 4:
                            context.startActivity(new Intent(context, TargetActivity.class));

                            break;
                        case 5:
                            context.startActivity(new Intent(context, StockActivity.class));
                            break;

                        case 6:
                            context.startActivity(new Intent(context, ProductionActivity.class));


                            break;
                        case 7:
                            context.startActivity(new Intent(context, PurchaseActivity.class));

                            break;

                        case 8:
                            context.startActivity(new Intent(context, DiscountsActivity.class));

                            break;


                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public HomeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_homeadapter,parent,false);

        return new HomeHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeHolder holder, int position) {
        holder.txtdata.setText(arr[position]);
        holder.img.setImageResource(Literals.imgid[position]);

    }

    @Override
    public int getItemCount() {
        return arr.length;
    }
}
