package com.pgpavers.admin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.data.pojo.OutStandingCustomer;
import com.pgpavers.admin.fragments.CustomerFragment;

import java.util.List;

public class CustomerOutstandAdapter extends RecyclerView.Adapter<CustomerOutstandAdapter.CustomerHolder> {


    CustomerFragment customerFragment;
    Context context;
    List<OutStandingCustomer>outStandingCustomers;

    public CustomerOutstandAdapter(Context context, List<OutStandingCustomer> outStandingCustomers, CustomerFragment customerFragment) {
        this.context = context;
        this.outStandingCustomers = outStandingCustomers;
        this.customerFragment=customerFragment;
    }

    public class CustomerHolder extends RecyclerView.ViewHolder{

        TextView txtdetails;

        public CustomerHolder(@NonNull View itemView) {
            super(itemView);
            txtdetails=itemView.findViewById(R.id.txtdetails);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    customerFragment.selectCustomerOutStand(outStandingCustomers.get(getAdapterPosition()));

                }
            });
        }
    }


    @NonNull
    @Override
    public CustomerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_productionadapter,parent,false);

        return new CustomerHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomerHolder holder, int position) {

        holder.txtdetails.setText("Sales head : "+outStandingCustomers.get(position).getCustomer_outstanding_under_sales_head()+"\n"+
                "Credited amount : "+outStandingCustomers.get(position).getVlAdvance()+"\n"+
                "Debited amount : "+outStandingCustomers.get(position).getVlBalance());

    }

    @Override
    public int getItemCount() {
        return outStandingCustomers.size();
    }
}
