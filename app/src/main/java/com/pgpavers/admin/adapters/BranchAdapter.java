package com.pgpavers.admin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.data.pojo.Branchdata;

import java.util.List;

public class BranchAdapter extends RecyclerView.Adapter<BranchAdapter.BranchHolder> {

    Context context;
    List<Branchdata>branchdata;

    public BranchAdapter(Context context, List<Branchdata> branchdata) {
        this.context = context;
        this.branchdata = branchdata;
    }

    public class BranchHolder extends RecyclerView.ViewHolder
    {
        TextView txtPattern;
        public BranchHolder(@NonNull View itemView) {
            super(itemView);
            txtPattern=itemView.findViewById(R.id.txtPattern);
        }
    }

    @NonNull
    @Override
    public BranchHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_stockadapter,parent,false);

        return new BranchHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BranchHolder holder, int position) {

        holder.txtPattern.setText(branchdata.get(position).getDsBranch()+"\n Qty : "+branchdata.get(position).getNrQty());

    }

    @Override
    public int getItemCount() {
        return branchdata.size();
    }
}
