package com.pgpavers.admin.adapters;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.pgpavers.admin.fragments.SalesBranchFragment;
import com.pgpavers.admin.fragments.SalesFragment;

public class SalesTabPagerAdapter extends FragmentPagerAdapter {

    String arr[]={"Sales person","Branch"};

    public SalesTabPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment=null;

        if(position==0)
        {
            fragment=new SalesFragment();
        }
        else {

            fragment=new SalesBranchFragment();
        }


        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return arr[position];
    }
}
