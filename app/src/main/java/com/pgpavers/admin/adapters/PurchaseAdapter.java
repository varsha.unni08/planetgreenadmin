package com.pgpavers.admin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.data.pojo.Purchase;

import java.util.List;

public class PurchaseAdapter extends RecyclerView.Adapter<PurchaseAdapter.Purchaseholder> {


    Context context;
    List<Purchase>purchases;

    public PurchaseAdapter(Context context, List<Purchase> purchases) {
        this.context = context;
        this.purchases = purchases;
    }

    public class Purchaseholder extends RecyclerView.ViewHolder{

        TextView txtdetails;

        public Purchaseholder(@NonNull View itemView) {
            super(itemView);
            txtdetails=itemView.findViewById(R.id.txtdetails);
        }
    }

    @NonNull
    @Override
    public Purchaseholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_productionadapter,parent,false);


        return new Purchaseholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Purchaseholder holder, int position) {

        holder.txtdetails.setText("Supplier : "+purchases.get(position).getDsSupplier()+
                "\nInvoice : "+purchases.get(position).getDsInvoice()+
                        "\nDate of invoice : "+purchases.get(position).getDsInvoice()+
                "\nGrand Total : "+purchases.get(position).getVlGrandTotal()
                );

    }

    @Override
    public int getItemCount() {
        return purchases.size();
    }
}
