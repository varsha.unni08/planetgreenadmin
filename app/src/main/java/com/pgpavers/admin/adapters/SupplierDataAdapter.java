package com.pgpavers.admin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pgpavers.admin.R;
import com.pgpavers.admin.data.pojo.SupplierData;

import java.util.List;

public class SupplierDataAdapter extends RecyclerView.Adapter<SupplierDataAdapter.SupplierHolder> {

    Context context;
    List<SupplierData>supplierData;

    public SupplierDataAdapter(Context context, List<SupplierData> supplierData) {
        this.context = context;
        this.supplierData = supplierData;
    }

    public class SupplierHolder extends RecyclerView.ViewHolder{

        TextView txtdetails;

        public SupplierHolder(@NonNull View itemView) {
            super(itemView);
            txtdetails=itemView.findViewById(R.id.txtdetails);
        }
    }


    @NonNull
    @Override
    public SupplierHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_productionadapter,parent,false);



        return new SupplierHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SupplierHolder holder, int position) {

        holder.txtdetails.setText("Branch : "+supplierData.get(position).getDsBranch()+"\n"
        +"Branch code : "+supplierData.get(position).getDsBranchCode()+"\n"+
                "Total Advance : "+supplierData.get(position).getTotalAdvance()+"\n"+
                "Total balance : "+supplierData.get(position).getTotalBalance());

    }

    @Override
    public int getItemCount() {
        return supplierData.size();
    }
}
