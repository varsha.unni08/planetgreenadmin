package com.pgpavers.admin.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OutStandingCustomer implements Serializable {

    @SerializedName("cd_sales_person")
    @Expose
    private String cdSalesPerson="";
    @SerializedName("name_sales_person")
    @Expose
    private String nameSalesPerson="";
    @SerializedName("vl_advance")
    @Expose
    private String vlAdvance="";
    @SerializedName("vl_balance")
    @Expose
    private String vlBalance="";
    @SerializedName("message")
    @Expose
    private String message="";
    @SerializedName("customer_outstanding_under_sales_head")
    @Expose
    private String customer_outstanding_under_sales_head="";



    public OutStandingCustomer() {
    }

    public String getCustomer_outstanding_under_sales_head() {
        return customer_outstanding_under_sales_head;
    }

    public void setCustomer_outstanding_under_sales_head(String customer_outstanding_under_sales_head) {
        this.customer_outstanding_under_sales_head = customer_outstanding_under_sales_head;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCdSalesPerson() {
        return cdSalesPerson;
    }

    public void setCdSalesPerson(String cdSalesPerson) {
        this.cdSalesPerson = cdSalesPerson;
    }

    public String getNameSalesPerson() {
        return nameSalesPerson;
    }

    public void setNameSalesPerson(String nameSalesPerson) {
        this.nameSalesPerson = nameSalesPerson;
    }

    public String getVlAdvance() {
        return vlAdvance;
    }

    public void setVlAdvance(String vlAdvance) {
        this.vlAdvance = vlAdvance;
    }

    public String getVlBalance() {
        return vlBalance;
    }

    public void setVlBalance(String vlBalance) {
        this.vlBalance = vlBalance;
    }
}
