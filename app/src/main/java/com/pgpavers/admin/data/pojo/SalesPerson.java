package com.pgpavers.admin.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalesPerson {

    @SerializedName("cd_sales_person")
    @Expose
    private String cdSalesPerson="";
    @SerializedName("ds_sales_person")
    @Expose
    private String dsSalesPerson="";

    public SalesPerson() {
    }

    public String getCdSalesPerson() {
        return cdSalesPerson;
    }

    public void setCdSalesPerson(String cdSalesPerson) {
        this.cdSalesPerson = cdSalesPerson;
    }

    public String getDsSalesPerson() {
        return dsSalesPerson;
    }

    public void setDsSalesPerson(String dsSalesPerson) {
        this.dsSalesPerson = dsSalesPerson;
    }
}
