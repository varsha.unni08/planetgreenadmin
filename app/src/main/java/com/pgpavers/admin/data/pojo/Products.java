package com.pgpavers.admin.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Products {

    @SerializedName("cd_product")
    @Expose
    private String cdProduct="";
    @SerializedName("ds_product")
    @Expose
    private String dsProduct="";

    public Products() {
    }

    public String getCdProduct() {
        return cdProduct;
    }

    public void setCdProduct(String cdProduct) {
        this.cdProduct = cdProduct;
    }

    public String getDsProduct() {
        return dsProduct;
    }

    public void setDsProduct(String dsProduct) {
        this.dsProduct = dsProduct;
    }
}
