package com.pgpavers.admin.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Productiondata implements Serializable {

    private String dsBranch="";

    private String dsBranchCode="";

    private Integer branchwiseProducedQty=0;


    public Productiondata() {
    }

    public String getDsBranch() {
        return dsBranch;
    }

    public void setDsBranch(String dsBranch) {
        this.dsBranch = dsBranch;
    }

    public String getDsBranchCode() {
        return dsBranchCode;
    }

    public void setDsBranchCode(String dsBranchCode) {
        this.dsBranchCode = dsBranchCode;
    }

    public Integer getBranchwiseProducedQty() {
        return branchwiseProducedQty;
    }

    public void setBranchwiseProducedQty(Integer branchwiseProducedQty) {
        this.branchwiseProducedQty = branchwiseProducedQty;
    }
}
