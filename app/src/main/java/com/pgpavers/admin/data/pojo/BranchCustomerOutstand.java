package com.pgpavers.admin.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BranchCustomerOutstand  implements Serializable {

    @SerializedName("cd_customer")
    @Expose
    private String cdCustomer="";
    @SerializedName("ds_customer")
    @Expose
    private String dsCustomer="";
    @SerializedName("vl_advance")
    @Expose
    private String vlAdvance="";
    @SerializedName("vl_balance")
    @Expose
    private String vlBalance="";

    public BranchCustomerOutstand() {
    }

    public String getCdCustomer() {
        return cdCustomer;
    }

    public void setCdCustomer(String cdCustomer) {
        this.cdCustomer = cdCustomer;
    }

    public String getDsCustomer() {
        return dsCustomer;
    }

    public void setDsCustomer(String dsCustomer) {
        this.dsCustomer = dsCustomer;
    }

    public String getVlAdvance() {
        return vlAdvance;
    }

    public void setVlAdvance(String vlAdvance) {
        this.vlAdvance = vlAdvance;
    }

    public String getVlBalance() {
        return vlBalance;
    }

    public void setVlBalance(String vlBalance) {
        this.vlBalance = vlBalance;
    }
}
