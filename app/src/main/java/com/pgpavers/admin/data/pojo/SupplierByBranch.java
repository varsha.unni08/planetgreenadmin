package com.pgpavers.admin.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SupplierByBranch {

    @SerializedName("cd_branch")
    @Expose
    private String cdBranch="";
    @SerializedName("cd_supplier")
    @Expose
    private String cdSupplier="";
    @SerializedName("ds_supplier")
    @Expose
    private String dsSupplier="";
    @SerializedName("total_advance")
    @Expose
    private String totalAdvance="";
    @SerializedName("total_balance")
    @Expose
    private String totalBalance="";

    public SupplierByBranch() {
    }

    public String getCdBranch() {
        return cdBranch;
    }

    public void setCdBranch(String cdBranch) {
        this.cdBranch = cdBranch;
    }

    public String getCdSupplier() {
        return cdSupplier;
    }

    public void setCdSupplier(String cdSupplier) {
        this.cdSupplier = cdSupplier;
    }

    public String getDsSupplier() {
        return dsSupplier;
    }

    public void setDsSupplier(String dsSupplier) {
        this.dsSupplier = dsSupplier;
    }

    public String getTotalAdvance() {
        return totalAdvance;
    }

    public void setTotalAdvance(String totalAdvance) {
        this.totalAdvance = totalAdvance;
    }

    public String getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(String totalBalance) {
        this.totalBalance = totalBalance;
    }
}
