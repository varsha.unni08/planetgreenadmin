package com.pgpavers.admin.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalesPersonWiseSales {

    @SerializedName("cd_branch")
    @Expose
    private String cdBranch="";
    @SerializedName("ds_branch_code")
    @Expose
    private String dsBranchCode="";
    @SerializedName("series")
    @Expose
    private String series="";
    @SerializedName("ds_customer")
    @Expose
    private String dsCustomer="";
    @SerializedName("ds_sales_invoice")
    @Expose
    private String dsSalesInvoice="";
    @SerializedName("dt_product_sale")
    @Expose
    private String dtProductSale="";
    @SerializedName("vl_grand_total")
    @Expose
    private String vlGrandTotal="";

    @SerializedName("grand_total")
    @Expose
    private Integer grandTotal=0;
    @SerializedName("status")
    @Expose
    private Integer status=1;
    @SerializedName("messgae")
    @Expose
    private String messgae="";

    public SalesPersonWiseSales() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessgae() {
        return messgae;
    }

    public void setMessgae(String messgae) {
        this.messgae = messgae;
    }

    public String getCdBranch() {
        return cdBranch;
    }

    public void setCdBranch(String cdBranch) {
        this.cdBranch = cdBranch;
    }

    public String getDsBranchCode() {
        return dsBranchCode;
    }

    public void setDsBranchCode(String dsBranchCode) {
        this.dsBranchCode = dsBranchCode;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getDsCustomer() {
        return dsCustomer;
    }

    public void setDsCustomer(String dsCustomer) {
        this.dsCustomer = dsCustomer;
    }

    public String getDsSalesInvoice() {
        return dsSalesInvoice;
    }

    public void setDsSalesInvoice(String dsSalesInvoice) {
        this.dsSalesInvoice = dsSalesInvoice;
    }

    public String getDtProductSale() {
        return dtProductSale;
    }

    public void setDtProductSale(String dtProductSale) {
        this.dtProductSale = dtProductSale;
    }

    public String getVlGrandTotal() {
        return vlGrandTotal;
    }

    public void setVlGrandTotal(String vlGrandTotal) {
        this.vlGrandTotal = vlGrandTotal;
    }

    public Integer getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Integer grandTotal) {
        this.grandTotal = grandTotal;
    }
}
