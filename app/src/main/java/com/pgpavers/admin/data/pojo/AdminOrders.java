package com.pgpavers.admin.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdminOrders {
    int seolected=0;

    @SerializedName("cd_sales_order")
    @Expose
    private String cdSalesOrder="";
    @SerializedName("cd_branch")
    @Expose
    private String cdBranch="";
    @SerializedName("dt_delivery")
    @Expose
    private String dtDelivery="";
    @SerializedName("cd_customer")
    @Expose
    private String cdCustomer="";
    @SerializedName("ds_customer")
    @Expose
    private String dsCustomer="";
    @SerializedName("cd_sales_person")
    @Expose
    private String cdSalesPerson="";
    @SerializedName("ds_sales_person")
    @Expose
    private String dsSalesPerson="";
    @SerializedName("vl_total")
    @Expose
    private String vlTotal="";

    public AdminOrders() {
    }

    public int getSeolected() {
        return seolected;
    }

    public void setSeolected(int seolected) {
        this.seolected = seolected;
    }

    public String getCdSalesOrder() {
        return cdSalesOrder;
    }

    public void setCdSalesOrder(String cdSalesOrder) {
        this.cdSalesOrder = cdSalesOrder;
    }

    public String getCdBranch() {
        return cdBranch;
    }

    public void setCdBranch(String cdBranch) {
        this.cdBranch = cdBranch;
    }

    public String getDtDelivery() {
        return dtDelivery;
    }

    public void setDtDelivery(String dtDelivery) {
        this.dtDelivery = dtDelivery;
    }

    public String getCdCustomer() {
        return cdCustomer;
    }

    public void setCdCustomer(String cdCustomer) {
        this.cdCustomer = cdCustomer;
    }

    public String getDsCustomer() {
        return dsCustomer;
    }

    public void setDsCustomer(String dsCustomer) {
        this.dsCustomer = dsCustomer;
    }

    public String getCdSalesPerson() {
        return cdSalesPerson;
    }

    public void setCdSalesPerson(String cdSalesPerson) {
        this.cdSalesPerson = cdSalesPerson;
    }

    public String getDsSalesPerson() {
        return dsSalesPerson;
    }

    public void setDsSalesPerson(String dsSalesPerson) {
        this.dsSalesPerson = dsSalesPerson;
    }

    public String getVlTotal() {
        return vlTotal;
    }

    public void setVlTotal(String vlTotal) {
        this.vlTotal = vlTotal;
    }
}
