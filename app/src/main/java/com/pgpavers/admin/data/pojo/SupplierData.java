package com.pgpavers.admin.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SupplierData {

    @SerializedName("status")
    @Expose
    private Integer status=0;
    @SerializedName("message")
    @Expose
    private String message="";
    @SerializedName("total_advance")
    @Expose
    private String totalAdvance="";
    @SerializedName("total_balance")
    @Expose
    private String totalBalance="";

    @SerializedName("cd_branch")
    @Expose
    private String cdBranch="";
    @SerializedName("ds_branch")
    @Expose
    private String dsBranch="";
    @SerializedName("ds_branch_code")
    @Expose
    private String dsBranchCode="";


    public SupplierData() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTotalAdvance() {
        return totalAdvance;
    }

    public void setTotalAdvance(String totalAdvance) {
        this.totalAdvance = totalAdvance;
    }

    public String getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(String totalBalance) {
        this.totalBalance = totalBalance;
    }

    public String getCdBranch() {
        return cdBranch;
    }

    public void setCdBranch(String cdBranch) {
        this.cdBranch = cdBranch;
    }

    public String getDsBranch() {
        return dsBranch;
    }

    public void setDsBranch(String dsBranch) {
        this.dsBranch = dsBranch;
    }

    public String getDsBranchCode() {
        return dsBranchCode;
    }

    public void setDsBranchCode(String dsBranchCode) {
        this.dsBranchCode = dsBranchCode;
    }
}
