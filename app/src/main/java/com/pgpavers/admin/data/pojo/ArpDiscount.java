package com.pgpavers.admin.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ArpDiscount {

    @SerializedName("status")
    @Expose
    private Integer status=0;
    @SerializedName("from_date")
    @Expose
    private String fromDate="";
    @SerializedName("to_date")
    @Expose
    private String toDate="";
    @SerializedName("arp")
    @Expose
    private Double arp=0.0;

    public ArpDiscount() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public Double getArp() {
        return arp;
    }

    public void setArp(Double arp) {
        this.arp = arp;
    }
}
