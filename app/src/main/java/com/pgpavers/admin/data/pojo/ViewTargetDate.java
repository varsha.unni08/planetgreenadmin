package com.pgpavers.admin.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ViewTargetDate {

    @SerializedName("cd_reporting_head")
    @Expose
    private String cdReportingHead;
    @SerializedName("cd_sales_person")
    @Expose
    private String cdSalesPerson;
    @SerializedName("ds_sales_person")
    @Expose
    private String dsSalesPerson;
    @SerializedName("Target")
    @Expose
    private String target;
    @SerializedName("Achievement")
    @Expose
    private String achievement;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Target_total")
    @Expose
    private String targetTotal;
    @SerializedName("Achievement_total")
    @Expose
    private String achievementTotal;

    public ViewTargetDate() {
    }




    public String getCdReportingHead() {
        return cdReportingHead;
    }

    public void setCdReportingHead(String cdReportingHead) {
        this.cdReportingHead = cdReportingHead;
    }

    public String getCdSalesPerson() {
        return cdSalesPerson;
    }

    public void setCdSalesPerson(String cdSalesPerson) {
        this.cdSalesPerson = cdSalesPerson;
    }

    public String getDsSalesPerson() {
        return dsSalesPerson;
    }

    public void setDsSalesPerson(String dsSalesPerson) {
        this.dsSalesPerson = dsSalesPerson;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getAchievement() {
        return achievement;
    }

    public void setAchievement(String achievement) {
        this.achievement = achievement;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTargetTotal() {
        return targetTotal;
    }

    public void setTargetTotal(String targetTotal) {
        this.targetTotal = targetTotal;
    }

    public String getAchievementTotal() {
        return achievementTotal;
    }

    public void setAchievementTotal(String achievementTotal) {
        this.achievementTotal = achievementTotal;
    }
}
