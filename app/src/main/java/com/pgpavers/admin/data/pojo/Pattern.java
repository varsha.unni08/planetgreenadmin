package com.pgpavers.admin.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pattern {

    @SerializedName("cd_pattern")
    @Expose
    private String cdPattern="";
    @SerializedName("ds_pattern")
    @Expose
    private String dsPattern="";

    public Pattern() {
    }

    public String getCdPattern() {
        return cdPattern;
    }

    public void setCdPattern(String cdPattern) {
        this.cdPattern = cdPattern;
    }

    public String getDsPattern() {
        return dsPattern;
    }

    public void setDsPattern(String dsPattern) {
        this.dsPattern = dsPattern;
    }
}
