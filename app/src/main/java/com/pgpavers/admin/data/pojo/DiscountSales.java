package com.pgpavers.admin.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DiscountSales {

    @SerializedName("status")
    @Expose
    private Integer status=0;
    @SerializedName("message")
    @Expose
    private String message="";
    @SerializedName("Total discount")
    @Expose
    private Integer totalDiscount=0;

    public DiscountSales() {
    }

    public Integer getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(Integer totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
