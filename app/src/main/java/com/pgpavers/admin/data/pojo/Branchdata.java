package com.pgpavers.admin.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Branchdata implements Serializable {
    @SerializedName("cd_branch")
    @Expose
    private String cdBranch="";
    @SerializedName("ds_branch")
    @Expose
    private String dsBranch="";
    @SerializedName("nr_qty")
    @Expose
    private String nrQty="";

    public Branchdata() {
    }

    public String getCdBranch() {
        return cdBranch;
    }

    public void setCdBranch(String cdBranch) {
        this.cdBranch = cdBranch;
    }

    public String getDsBranch() {
        return dsBranch;
    }

    public void setDsBranch(String dsBranch) {
        this.dsBranch = dsBranch;
    }

    public String getNrQty() {
        return nrQty;
    }

    public void setNrQty(String nrQty) {
        this.nrQty = nrQty;
    }
}
