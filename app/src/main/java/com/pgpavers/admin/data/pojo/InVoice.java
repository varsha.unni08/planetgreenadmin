package com.pgpavers.admin.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InVoice {

    @SerializedName("cd_branch")
    @Expose
    private String cdBranch="";
    @SerializedName("ds_sales_invoice")
    @Expose
    private String dsSalesInvoice="";
    @SerializedName("dt_product_sale")
    @Expose
    private String dtProductSale="";
    @SerializedName("vl_grand_total")
    @Expose
    private String vlGrandTotal="";
    @SerializedName("vl_balance")
    @Expose
    private String vlBalance="";
    @SerializedName("ageing")
    @Expose
    private String ageing="";
    @SerializedName("Invoice details of")
    @Expose
    private String invoiceDetailsOf="";
    @SerializedName("cd_customer")
    @Expose
    private String cdCustomer="";

    public InVoice() {
    }

    public String getInvoiceDetailsOf() {
        return invoiceDetailsOf;
    }

    public void setInvoiceDetailsOf(String invoiceDetailsOf) {
        this.invoiceDetailsOf = invoiceDetailsOf;
    }

    public String getCdCustomer() {
        return cdCustomer;
    }

    public void setCdCustomer(String cdCustomer) {
        this.cdCustomer = cdCustomer;
    }

    public String getCdBranch() {
        return cdBranch;
    }

    public void setCdBranch(String cdBranch) {
        this.cdBranch = cdBranch;
    }

    public String getDsSalesInvoice() {
        return dsSalesInvoice;
    }

    public void setDsSalesInvoice(String dsSalesInvoice) {
        this.dsSalesInvoice = dsSalesInvoice;
    }

    public String getDtProductSale() {
        return dtProductSale;
    }

    public void setDtProductSale(String dtProductSale) {
        this.dtProductSale = dtProductSale;
    }

    public String getVlGrandTotal() {
        return vlGrandTotal;
    }

    public void setVlGrandTotal(String vlGrandTotal) {
        this.vlGrandTotal = vlGrandTotal;
    }

    public String getVlBalance() {
        return vlBalance;
    }

    public void setVlBalance(String vlBalance) {
        this.vlBalance = vlBalance;
    }

    public String getAgeing() {
        return ageing;
    }

    public void setAgeing(String ageing) {
        this.ageing = ageing;
    }
}
