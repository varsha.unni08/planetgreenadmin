package com.pgpavers.admin.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Purchase {
    @SerializedName("Total purchase")
    @Expose
    private Integer totalPurchase=0;
    @SerializedName("Type")
    @Expose
    private String type="";
    @SerializedName("cd_purchase")
    @Expose
    private String cdPurchase="";
    @SerializedName("ds_supplier")
    @Expose
    private String dsSupplier="";
    @SerializedName("ds_invoice")
    @Expose
    private String dsInvoice="";
    @SerializedName("dt_invoice")
    @Expose
    private String dtInvoice="";
    @SerializedName("vl_grand_total")
    @Expose
    private String vlGrandTotal="";


    public Purchase() {
    }

    public Integer getTotalPurchase() {
        return totalPurchase;
    }

    public void setTotalPurchase(Integer totalPurchase) {
        this.totalPurchase = totalPurchase;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCdPurchase() {
        return cdPurchase;
    }

    public void setCdPurchase(String cdPurchase) {
        this.cdPurchase = cdPurchase;
    }

    public String getDsSupplier() {
        return dsSupplier;
    }

    public void setDsSupplier(String dsSupplier) {
        this.dsSupplier = dsSupplier;
    }

    public String getDsInvoice() {
        return dsInvoice;
    }

    public void setDsInvoice(String dsInvoice) {
        this.dsInvoice = dsInvoice;
    }

    public String getDtInvoice() {
        return dtInvoice;
    }

    public void setDtInvoice(String dtInvoice) {
        this.dtInvoice = dtInvoice;
    }

    public String getVlGrandTotal() {
        return vlGrandTotal;
    }

    public void setVlGrandTotal(String vlGrandTotal) {
        this.vlGrandTotal = vlGrandTotal;
    }
}
