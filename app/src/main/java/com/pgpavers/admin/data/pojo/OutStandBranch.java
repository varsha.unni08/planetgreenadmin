package com.pgpavers.admin.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OutStandBranch implements Serializable {

    @SerializedName("cd_branch")
    @Expose
    private String cdBranch="";
    @SerializedName("ds_branch")
    @Expose
    private String dsBranch="";
    @SerializedName("vl_advance")
    @Expose
    private String vlAdvance="";
    @SerializedName("vl_balance")
    @Expose
    private String vlBalance="";
    @SerializedName("total_advance")
    @Expose
    private String totalAdvance="";
    @SerializedName("total_balance")
    @Expose
    private String totalBalance="";

    public OutStandBranch() {
    }

    public String getCdBranch() {
        return cdBranch;
    }

    public void setCdBranch(String cdBranch) {
        this.cdBranch = cdBranch;
    }

    public String getDsBranch() {
        return dsBranch;
    }

    public void setDsBranch(String dsBranch) {
        this.dsBranch = dsBranch;
    }

    public String getVlAdvance() {
        return vlAdvance;
    }

    public void setVlAdvance(String vlAdvance) {
        this.vlAdvance = vlAdvance;
    }

    public String getVlBalance() {
        return vlBalance;
    }

    public void setVlBalance(String vlBalance) {
        this.vlBalance = vlBalance;
    }

    public String getTotalAdvance() {
        return totalAdvance;
    }

    public void setTotalAdvance(String totalAdvance) {
        this.totalAdvance = totalAdvance;
    }

    public String getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(String totalBalance) {
        this.totalBalance = totalBalance;
    }
}
