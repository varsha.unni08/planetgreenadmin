package com.pgpavers.admin.data.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BranchListData implements Serializable {

    List<Branchdata>branchdata=new ArrayList<>();

    public List<Branchdata> getBranchdata() {
        return branchdata;
    }

    public void setBranchdata(List<Branchdata> branchdata) {
        this.branchdata = branchdata;
    }
}
