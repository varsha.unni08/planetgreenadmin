package com.pgpavers.admin.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalesPersonOutStanding {

    @SerializedName("cd_sales_person")
    @Expose
    private String cdSalesPerson="";
    @SerializedName("customer_outstanding_under_salesperson")
    @Expose
    private String customerOutstandingUnderSalesperson="";
    @SerializedName("vl_advance")
    @Expose
    private String vlAdvance="";
    @SerializedName("vl_balance")
    @Expose
    private String vlBalance="";

    public SalesPersonOutStanding() {
    }

    public String getCdSalesPerson() {
        return cdSalesPerson;
    }

    public void setCdSalesPerson(String cdSalesPerson) {
        this.cdSalesPerson = cdSalesPerson;
    }

    public String getCustomerOutstandingUnderSalesperson() {
        return customerOutstandingUnderSalesperson;
    }

    public void setCustomerOutstandingUnderSalesperson(String customerOutstandingUnderSalesperson) {
        this.customerOutstandingUnderSalesperson = customerOutstandingUnderSalesperson;
    }

    public String getVlAdvance() {
        return vlAdvance;
    }

    public void setVlAdvance(String vlAdvance) {
        this.vlAdvance = vlAdvance;
    }

    public String getVlBalance() {
        return vlBalance;
    }

    public void setVlBalance(String vlBalance) {
        this.vlBalance = vlBalance;
    }
}
